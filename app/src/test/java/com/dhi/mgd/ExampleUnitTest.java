package com.dhi.mgd;

import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }

    /*
    @Test
    public void request_json_test() {
        ArrayList<Question> questionArray = new ArrayList<Question>();
        LinkedHashMap <String, String> optQ1 = new LinkedHashMap<String, String>();
        optQ1.put("male", "Male");
        optQ1.put("male", "Male");

        Question q1 = new Question("1", "gender", "What Gender are you?", QuestionType.BOOL, optQ1);
        ArrayList<String> selectedOptions = new ArrayList<String>();
        selectedOptions.add("male");
        q1.setArrayResponse(selectedOptions);
        questionArray.add(q1);
        String jsonStr = JSONGenerator.generateRequestJSON(questionArray);
        if (jsonStr == null) {
            fail("No response obtained");
            return;
        }
        System.out.println(jsonStr);
        assertEquals(4, 2 + 2);
    }

    @Test
    public void assessment_parse_test() {
        String jsonStr = "[{\"responses\":[{\"age.days.value\":0},{\"gender.male.selected\":true},{\"heart_rate.bpm.value\":0},{\"respiratory_rate.bpm.value\":0},{\"temperature.Celsius.value\":0},{\"vomiting_severity.none.selected\":true},{\"weight.lbs.value\":0}],\"assessments\":[{\"id\":6,\"name\":\"malaria\",\"severity\":\"none\"}]}]";
        ArrayList<AssessmentScore> parseResp = JSONGenerator.parseAssessmentResponse(jsonStr);
        assertEquals(parseResp.size(), 1);
    }
    */
}