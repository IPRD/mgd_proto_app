package com.dhi.OxiThermo;

import java.util.ArrayList;
import java.util.Arrays;
/**
 *.
 * Date: Aug 30 2019
 * @author Apra Developers
 * @version 1.0
 */
public class ModeMedian {

    /**
     *
     * This method firstly sort the array and returns middle element if the length of arrray is odd.Otherwise it will return the average of two miidele elements.
     * @param arrayList
     * @return
     */

    public static Float findMedian(ArrayList<Float> arrayList) {

        int length=arrayList.size();
        float res = 0;
        Float[] sort = new Float[length];
        Float[] array = arrayList.toArray(new Float[arrayList.size()]);
        System.arraycopy(array, 0, sort, 0, sort.length);
        Arrays.sort(sort);
        if (length % 2 == 0) {
             res =(sort[(sort.length / 2) - 1] + sort[sort.length / 2]) / 2;
        } else {
            res = sort[sort.length / 2];
        }
        return res;
    }

    /**
     *
     * This method counts the occurrence of each element of array and return the lement which has the maximum count.
     * @param array
     * @return
     */
    public static Float findMode(ArrayList<Float> array) {

        int maxCount = 0;
        float res = 0;
        int length=array.size();

        for (int i = 0; i <length; ++i) {

            int count = 0;
            for (int j = 0; j <length; ++j) {

                if (array.get(j) == array.get(i)) ++count;
            }

            if (count > maxCount)
            {
                maxCount = count;
                res = array.get(i);
            }
        }
        return res;
    }
}