package com.dhi.OxiThermo;

import android.Manifest;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.ParcelUuid;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.dhi.RequestResponse.Coding;
import com.dhi.RequestResponse.Identifier;
import com.dhi.RequestResponse.ResourceObservation;
import com.dhi.RequestResponse.Subject;
import com.dhi.RequestResponse.ValueQuantity;
import com.dhi.mgd.EmptyPlaceholders;
import com.dhi.mgd.PlaceholderState;
import com.dhi.mgd.R;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 *.
 * Date: Aug 30 2019
 * @author Apra Developers
 * @version 1.0
 */
public class OximeterOrThermometer extends AppCompatActivity {
    public final static String JumperservicesThermometer="0000fff0-0000-1000-8000-00805f9b34fb";//"cdeacb80-5235-4c07-8846-93a37ee6b86d";
    public final static String JumperDevReaderThermometer = "0000fff3-0000-1000-8000-00805f9b34fb";//"cdeacb81-5235-4c07-8846-93a37ee6b86d";

    public final static String JumperservicesOximeter="cdeacb80-5235-4c07-8846-93a37ee6b86d";
    public final static String JumperDevReaderOximeter = "cdeacb81-5235-4c07-8846-93a37ee6b86d";

    public final static int SelectedDeviceIndex =0;
    public final Map<String, String> mDeviceList=new HashMap<String ,String >();
    BluetoothManager btManager;
    BluetoothAdapter btAdapter;
    BluetoothLeScanner btScanner = null;
    BluetoothDevice mbtDevice=null;
    Button startScanningButton;
    Button stopScanningButton;
    Button backBtn;
    TextView peripheralTextView;
    TextView deviceNameView;
    Boolean mIntentcall=false;
    Boolean oxymeter=false,thermometer=false;

    private final static int REQUEST_ENABLE_BT = 1;
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;

    Boolean btScanning = false;
    int deviceIndex = 0;
    ArrayList<BluetoothDevice> devicesDiscovered = new ArrayList<BluetoothDevice>();
    EditText deviceIndexInput;
    Button connectToDevice;
    Button disconnectDevice;
    BluetoothGatt bluetoothGatt=null;

    public final static String ACTION_GATT_CONNECTED =
            "com.example.bluetooth.le.ACTION_GATT_CONNECTED";
    public final static String ACTION_GATT_DISCONNECTED =
            "com.example.bluetooth.le.ACTION_GATT_DISCONNECTED";
    public final static String ACTION_GATT_SERVICES_DISCOVERED =
            "com.example.bluetooth.le.ACTION_GATT_SERVICES_DISCOVERED";
    public final static String ACTION_DATA_AVAILABLE =
            "com.example.bluetooth.le.ACTION_DATA_AVAILABLE";
    public final static String EXTRA_DATA =
            "com.example.bluetooth.le.EXTRA_DATA";

    public Map<String, String> uuids = new HashMap<String, String>();
    ArrayList<PulseOrTemparatureInfo> deviceDataList = new ArrayList<>();

    // Stops scanning after 5 seconds.
    private Handler mHandler = new Handler();
    private static final long SCAN_PERIOD = 10000;
    static SpeakOut TTS=null;
    Context mCtx;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private Intent m_intent;

    final String HEARTRATE_CODE = "8867-4";
    final String SPO2_CODE = "59408-5";
    final String THINKMD_TEMPERATURE_CODE = "8310-5";
    String scannigdfor="Scanning ..";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oxi_temp);

        mDeviceList.clear();
        peripheralTextView = (TextView) findViewById(R.id.PeripheralTextView);
        peripheralTextView.setMovementMethod(new ScrollingMovementMethod());
        peripheralTextView.setVisibility(View.INVISIBLE);

        deviceNameView = (TextView) findViewById(R.id.DeviceName);
        deviceNameView.setText("");

        connectToDevice = (Button) findViewById(R.id.ConnectButton);
        connectToDevice.setVisibility(View.INVISIBLE);
        connectToDevice.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ConnectToBLEDevice();
            }
        });

        disconnectDevice = (Button) findViewById(R.id.DisconnectButton);
        disconnectDevice.setVisibility(View.INVISIBLE);
        disconnectDevice.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                disconnectDeviceSelected();
            }
        });

        startScanningButton = (Button) findViewById(R.id.StartScanButton);
        startScanningButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                startScanning();
            }
        });

        stopScanningButton = (Button) findViewById(R.id.StopScanButton);
        stopScanningButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                stopScanning();
                ArrayList l = new ArrayList();
            }
        });

        stopScanningButton.setVisibility(View.INVISIBLE);

        btManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        btAdapter = btManager.getAdapter();
        btScanner = btAdapter.getBluetoothLeScanner();

        if (btAdapter != null && !btAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
        }
        // Make sure we have access coarse location enabled, if not, prompt the user to enable it
        if (this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("This app needs location access");
            builder.setMessage("Please grant l boolean oxymeter=false,thermometer=false;ocation access so this app can detect peripherals.");
            builder.setPositiveButton(android.R.string.ok, null);
            builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                @Override
                public void onDismiss(DialogInterface dialog) {
                    requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);
                }
            });
            builder.show();
        }
        String intentmsg = getIntent().getStringExtra("serviceRequest");

        if(intentmsg != null){

            try {
                JSONObject reqJSON = new JSONObject(intentmsg);
                JSONArray arr = reqJSON.getJSONArray("entry");//tests
                for(int i=0; i < arr.length(); i++) {
                    JSONObject ob = (JSONObject) arr.get(i);
                    String svcLOINCCode = ob.getJSONObject("resource").getJSONObject("code").getJSONArray("coding").getJSONObject(0).getString("code");
                            //ob.getJSONObject("code").getJSONArray("coding").getJSONObject(0).getString("code");
                    Log.d("svcLOINCCode : " , svcLOINCCode);
                    if (svcLOINCCode.contentEquals(THINKMD_TEMPERATURE_CODE) ){//|| svcLOINCCode.contentEquals(THINKMD_TEMPERATURE_CODE)
                       Log.d("aaaa ", scannigdfor);
                        if(!scannigdfor.contains("My Thermometer"))
                        scannigdfor = "Use Thermometer";
                        thermometer=true;
                    }else if (svcLOINCCode.contentEquals(HEARTRATE_CODE)){
                        if(!scannigdfor.contains("My Oximeter"))
                        scannigdfor = "Use Oximeter";
                        oxymeter=true;
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            mCtx = getApplicationContext();
            if(TTS == null) TTS = new SpeakOut(mCtx);

            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    TTS.Speak("Please "+scannigdfor);
                }
            }, 500);

            Log.d("thermometer :"+thermometer, "oxymeter :"+oxymeter);
            startScanningButton.setVisibility(View.INVISIBLE);
            stopScanningButton.setVisibility(View.VISIBLE);
            connectToDevice.setVisibility(View.INVISIBLE);
            disconnectDevice.setVisibility(View.INVISIBLE);
            mIntentcall=true;
        }

        startScanning();

        ImageButton bckButton = (ImageButton) findViewById(R.id.emptyholderbckBtn1);
        bckButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent i = null;
                Class<? extends AppCompatActivity> destClass = null;
                PlaceholderState p = PlaceholderState.NULL;
                i = new Intent(OximeterOrThermometer.this, EmptyPlaceholders.class);
                p = PlaceholderState.PATIENT_DEVICES_MEASUREMENT;

                i.putExtras(getIntent());
                i.putExtra(getResources().getString(R.string.placehoder_enum), p);
                if(getIntent().getStringExtra("serviceRequest")!= null){
                    unRegisterAndCloseConnection();
                }
                finish();
                startActivity(i);
            }
        });
    }


    private ScanCallback leScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            String s = result.getDevice().getAddress().toString();
            Log.d("Mad..1>", s == null ? "" : s);
            List<ParcelUuid> p = result.getScanRecord().getServiceUuids();
           // Log.d("result.getScanRecord().getServiceUuids() : =",result.getScanRecord().getServiceUuids().toString());
            if(p != null){
                Log.d("#mDeviceList#####123##",mDeviceList.toString());
                for(int i=0;i< p.size();i++){
                    Log.d(s+" >>>>>>>>>123>>>>",p.get(i).toString());
                    if(JumperservicesThermometer.contentEquals(p.get(i).toString())
                            || JumperservicesOximeter.contentEquals(p.get(i).toString())){
                        Log.d("@@@","----------1");
                        if(!mDeviceList.containsKey(s))
                        {
                            Log.d("qqqqqqqqqqq","----------2");
                            mbtDevice = result.getDevice();
                            System.out.println("++++++++++"+mbtDevice.getName());
                            if(mbtDevice.getName().contains("Thermometer") && thermometer ){
                                deviceAddedAndConnectBLE(s);
                            }else if(mbtDevice.getName().contains("Oximeter") && oxymeter ){
                                deviceAddedAndConnectBLE(s);
                            }else{
                                Log.d("wrong device : ","select proper device");
                                if(thermometer)
                                    deviceNameView.setText("Looking for Thermometer... \n");
                                else if(oxymeter)
                                    deviceNameView.setText("Looking for Oximeter...\n");
                                else
                                    deviceNameView.setText("");
                            }
                        }
                    }
                }
            }
        }
    };

    private void deviceAddedAndConnectBLE(String s) {
        mDeviceList.put(s,mbtDevice.getName());
        Log.d(">1>>>>>>>>>>>(JumperservicesThermometer)",JumperservicesThermometer+" >>>>" +
                "JumperservicesOximeter"+JumperservicesOximeter);
        deviceNameView.setText("Name: " + mbtDevice.getName() +"\n");
        devicesDiscovered.add(mbtDevice);
        deviceIndex++;
        ConnectToBLEDevice();
    }



    private final BluetoothGattCallback btleGattCallback = new BluetoothGattCallback() {
        /**
         * Device connect call back
         * @param gatt
         * @param characteristic
         */
        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, final BluetoothGattCharacteristic characteristic) {
            // this will get called anytime you perform a read or write characteristic operation
            Log.d("OOOOOOOOOOOOOOOOy","OOOOOOOOOOOOOOOOy");
            final byte[] data = characteristic.getValue();
            String result="";
            if (data != null && data.length > 0) {
                final StringBuilder stringBuilder = new StringBuilder(data.length);

                for(byte byteChar : data)
                    stringBuilder.append(String.format("%02X ", byteChar));
                System.out.println(stringBuilder);
                byte hexaa= (byte) 0xAA;
                byte hex55= (byte) 0x55;
                byte hex81= (byte) 0x81;
                byte hex80= (byte) 0x80;
                if((data[0] == hexaa) && (data.length >=4) && thermometer) {
                    //deviceNameView.setText("Use Thermometer \n");
                    //--
                    String serviceRequest = getIntent().getStringExtra("serviceRequest");
                    result = getDeviceResults("THERMOMETER",data, serviceRequest);

                    if(serviceRequest != null){
                        unRegisterAndCloseConnection();
                    }
                    //--
                    //result = getThermometerData(data);

                }else if((data[0] == hex81) && (data.length >=4) && oxymeter){
                   // deviceNameView.setText("Use Oxymeter \n");
                    Log.d("OOOOOOOOOOOOOOOOx","OOOOOOOOOOOOOOOOx");
                    String serviceRequest = getIntent().getStringExtra("serviceRequest");
                    result = getDeviceResults("JUMPER",data ,serviceRequest);
                    //Log.d(">>>>>>>getDeviceResults>>>>>>>",result);

                    //deviceDataList.removeIf(s->s.equals(""));
                    if(serviceRequest != null && deviceDataList.size() > 10){
                        unRegisterAndCloseConnection();
                    }
                    //result = getOximeterData(data);

                }else if( data[0] ==hex80){
                    System.out.println("Plethysmogram");
                }
            }

            final String finalResult = result == null ? "" : result  ;

            runOnUiThread(new Runnable() {
                public void run() {

                    if(finalResult != null && finalResult.length()>0){
                        deviceNameView.setText("Name: " + mbtDevice.getName() +"\n");
                        Log.d("finalResult->>>-- Mad",finalResult);
                        peripheralTextView.setVisibility(View.VISIBLE);
                        peripheralTextView.append(finalResult);
                    }

                    final int scrollAmount = peripheralTextView.getLayout().getLineTop(peripheralTextView.getLineCount()) - peripheralTextView.getHeight();
                    // if there is no need to scroll, scrollAmount will be <=0
                    if (scrollAmount > 0) {
                        peripheralTextView.scrollTo(0, scrollAmount);
                    }
                }
            });
        }

        /**
         * If connection state is changes, trying to discover the bluetooth services.
         * @param gatt
         * @param status
         * @param newState
         */
        @Override
        public void onConnectionStateChange(final BluetoothGatt gatt, final int status, final int newState) {
            // this will get called when a device connects or disconnects
            System.out.println("onConnectionStateChange " + newState);
            switch (newState) {
                case 0:
                    mDeviceList.remove(gatt.getDevice().getAddress());
                    gatt.getDevice().getAddress();
                    OximeterOrThermometer.this.runOnUiThread(new Runnable() {
                        public void run() {
                            //if(!mIntentcall)
                            connectToDevice.setVisibility(View.INVISIBLE);
                            disconnectDevice.setVisibility(View.INVISIBLE);
                        }
                    });
                    break;
                case 2:
                    OximeterOrThermometer.this.runOnUiThread(new Runnable() {
                        public void run() {
                            connectToDevice.setVisibility(View.INVISIBLE);
                            if(!mIntentcall)  disconnectDevice.setVisibility(View.VISIBLE);
                        }
                    });
                    // discover services and characteristics for this device
                    bluetoothGatt.discoverServices();
                    break;
                default:
                    OximeterOrThermometer.this.runOnUiThread(new Runnable() {
                        public void run() {
                            peripheralTextView.append("we encounterned an unknown state, uh oh\n");
                        }
                    });
                    break;
            }
        }

        /**
         * On service id discovered, displays and register the service.
         * @param gatt : BluetoothGatt
         * @param status : status (int)
         */
        @Override
        public void onServicesDiscovered(final BluetoothGatt gatt, final int status) {
            // this will get called after the client initiates a 			BluetoothGatt.discoverServices() call
            OximeterOrThermometer.this.runOnUiThread(new Runnable() {
                public void run() {
                }
            });
            displayAndRegisterService(bluetoothGatt.getServices(),true);
        }

        /**
         *
         * Result of a characteristic read operation
         * @param gatt
         * @param characteristic
         * @param status
         */
        @Override
        public void onCharacteristicRead(BluetoothGatt gatt,
                                         BluetoothGattCharacteristic characteristic,
                                         int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                broadcastUpdate(ACTION_DATA_AVAILABLE, characteristic);
            }
        }
    };


    private String getDeviceResults(String deviceType , byte[] data, String serviceRequest){//

        HealthDeviceFactory hdf = new HealthDeviceFactory();
        HealthDevice hd =  hdf.getDevice(deviceType,data);
        PulseOrTemparatureInfo oximeterOrThermometer = hd.getDeviceResults();
        //System.out.println("oximeterOrThermometer.getResult() >>>"+oximeterOrThermometer.getResult());
        if(oximeterOrThermometer.getResult() != null && oximeterOrThermometer.getResult().length() > 0)
            deviceDataList.add(oximeterOrThermometer);

        return oximeterOrThermometer.getResult();
    }


    private void broadcastUpdate(final String action,
                                 final BluetoothGattCharacteristic characteristic) {
        System.out.println(characteristic.getUuid());
        Log.d("mad",characteristic.getUuid().toString());
    }

    /**
     *
     * Checking the required permission is given by user
     * @param requestCode
     * @param permissions : list of permissions
     * @param grantResults : permission results
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    System.out.println("coarse location permission granted");
                } else {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Functionality limited");
                    builder.setMessage("Since location access has not been granted, this app will not be able to discover beacons when in the background.");
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {

                        @Override
                        public void onDismiss(DialogInterface dialog) {
                        }
                    });
                    builder.show();
                }
                return;
            }
        }
    }

    /**
     * For Bluetooth device scanning
     */
    public void startScanning() {
        System.out.println("start scanning");
        btScanning = true;
        deviceIndex = 0;
        devicesDiscovered.clear();
        peripheralTextView.setText("");
        peripheralTextView.setVisibility(View.INVISIBLE);
        deviceNameView.setText(scannigdfor);
        startScanningButton.setVisibility(View.INVISIBLE);

        stopScanningButton.setVisibility(View.VISIBLE);
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                if(btScanner != null)
                btScanner.startScan(leScanCallback);
                System.out.println("===SCANNING===");
            }
        });
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                stopScanning();
            }
        }, SCAN_PERIOD);
    }

    /**
     * For stopint the bluetooth scanning
     */
    public void stopScanning() {
        System.out.println("stopping scanning");
        btScanning = false;

        runOnUiThread(new Runnable() {
            /**
             *
             */
            @Override
            public void run() {
                startScanningButton.setVisibility(View.VISIBLE);
                stopScanningButton.setVisibility(View.INVISIBLE);
            }
        });
        AsyncTask.execute(new Runnable() {
            /**
             *
             */
            @Override
            public void run() {
                btScanner.stopScan(leScanCallback);
            }
        });
        //if(!mIntentcall)startScanningButton.setVisibility(View.VISIBLE);

    }

    /**
     *  for connecting with registered device.
     */
    public void ConnectToBLEDevice(){
        runOnUiThread(new Runnable() {

            @Override
            public void run() {
                if(mCtx==null){
                    return;
                }
                    bluetoothGatt = mbtDevice.connectGatt(mCtx, false, btleGattCallback);
                    if(!mIntentcall)
                        connectToDevice.setVisibility(View.VISIBLE);
            }
        });
    }



    public void connectToDeviceSelected() {
        int deviceSelected = SelectedDeviceIndex;
        bluetoothGatt = devicesDiscovered.get(deviceSelected).connectGatt(this, false, btleGattCallback);
    }

    /**
     * For dis-connecting the conected device.
     */
    public void disconnectDeviceSelected() {
        if(bluetoothGatt!=null)bluetoothGatt.disconnect();
    }

    private void displayAndRegisterService(List<BluetoothGattService> gattServices,boolean register) {
        if (gattServices == null) return;
        // Loops through available GATT Services.
        for (BluetoothGattService gattService : gattServices) {
            final String uuid = gattService.getUuid().toString();
            System.out.println("Service discovered: " + uuid);

            new ArrayList<HashMap<String, String>>();
            List<BluetoothGattCharacteristic> gattCharacteristics =
                    gattService.getCharacteristics();

            // Loops through available Characteristics.
            for (BluetoothGattCharacteristic gattCharacteristic :
                    gattCharacteristics) {

                final String charUuid = gattCharacteristic.getUuid().toString();
                System.out.println("Characteristic discovered for service: " + charUuid);
                if(charUuid.contentEquals(JumperDevReaderOximeter) || charUuid.contentEquals(JumperDevReaderThermometer)){
                    bluetoothGatt.setCharacteristicNotification(gattCharacteristic,register);
                }
            }
        }
    }

    private void returnToCallingApp(ArrayList<PulseOrTemparatureInfo> deviceDataList){

        ModeMedian modeMedian = new ModeMedian();
        PulseOrTemparatureInfo ptinfo = new PulseOrTemparatureInfo();

        ResponsInfo responseInfor = new ResponsInfo();
        Intent intent = new Intent();
        try {
            if(deviceDataList.size() > 0 && deviceDataList.get(0).getPulse() != null) {
                ArrayList<Float> listOfPlse = ptinfo.getListOfPulse(deviceDataList);
                Float mode4pulse = modeMedian.findMode(listOfPlse);

                ArrayList<Float> listOfSp02 = ptinfo.getListOfSp02(deviceDataList);
                Float mode4sp02 = modeMedian.findMode(listOfSp02);

                resourceObservationInfo(deviceDataList, responseInfor, mode4pulse, "Heart rate", HEARTRATE_CODE, "beats/minute", "http://iprdgroup.com/FHIR/Resources/Observation2");
                resourceObservationInfo(deviceDataList, responseInfor, mode4sp02, "Oxygen saturation in Arterial blood by Pulse oximetry", SPO2_CODE, "%", "http://iprdgroup.com/FHIR/Resources/Observation3");

            }else if(deviceDataList.size() > 0 && deviceDataList.get(0).getTemparature() != null){
                Float median4temp = deviceDataList.get(0).getTemparature();
                resourceObservationInfo(deviceDataList, responseInfor, median4temp, "Body temperature", THINKMD_TEMPERATURE_CODE, "Fahrenheit", "http://iprdgroup.com/FHIR/Resources/Observation");
            }else{
                System.out.println("no device data list");
            }
            ObjectMapper mapper = new ObjectMapper();
            String jsonInString = mapper.writeValueAsString(responseInfor);
            Log.d("mad",jsonInString);
            intent.putExtra("observationResponse",jsonInString);

        } catch (Exception e) {
            e.printStackTrace();
            intent.putExtra("observationResponse",responseInfor.toString());
        }
        // Activity finished ok, return the data
        setResult(RESULT_OK, intent);
        finish();
    }

    private void resourceObservationInfo(ArrayList<PulseOrTemparatureInfo> deviceDataList, ResponsInfo responseInfor, Float median4temp, String s, String thinkmd_temperature_code, String fahrenheit, String s2) {
        ResourceObservation resourceObservationForTemperature
                = getResourceObservation(deviceDataList, s, thinkmd_temperature_code, fahrenheit, median4temp,
                "http://www.jumper.com", "usual", "5ff75a42-6820-4e14-b7ae-934ceda8d3d6");
        EntryObj entryObj = new EntryObj();
        entryObj.fullUrl = s2;
        entryObj.resource = resourceObservationForTemperature;
        responseInfor.entry.add(entryObj);
    }

    private ResourceObservation getResourceObservation(ArrayList<PulseOrTemparatureInfo> deviceDataList, String displayString, String codeString, String unitString, Float medianValue
        ,String systemName, String use, String value) {

        Log.d("mad",medianValue.toString());

        ResourceObservation resourceObservation = new ResourceObservation();
        resourceObservation.id= "Observation/obs1";

        //String pattern = "YYYY-MM-dd'T'hh:mm:ss+zz:zz";//"YYYY-MM-dd";
        String pattern = "YYYY-MM-dd'T'HH:mm:ss.SSSXXX";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
//new SimpleDateFormat("YYYY-MM-dd'T'HH:mm:ss.SSSZ")format(new Date());
        String date = simpleDateFormat.format(new Date());
        System.out.println(".........."+date);

        resourceObservation.effectiveDateTime = date;
        resourceObservation.status = "preliminary";
        Identifier identifier = new Identifier();
        identifier.system = systemName;
        identifier.use = use;
        identifier.value = value;
        identifier.type.coding.add(new Coding("http://loinc.org",codeString,displayString));

        resourceObservation.identifier.add(identifier);
        resourceObservation.subject = new Subject();
        Identifier identifierInSubject = new Identifier();
        identifierInSubject.system = systemName;
        identifierInSubject.use = use;
        identifierInSubject.value = value;

        identifierInSubject.type.coding.add(new Coding("http://loinc.org",codeString,displayString));

        resourceObservation.subject.identifier=identifierInSubject;
        Coding coding = new Coding("http://loinc.org",codeString,displayString);

        resourceObservation.code.coding.add(coding) ;
        resourceObservation.valueQuantity = valueQuantityData(medianValue, "http://unitsofmeasure.org", unitString);
        return resourceObservation;
    }

    private ValueQuantity valueQuantityData(Float modeOrMedian, String system, String unit) {
        ValueQuantity valueQuantity = new ValueQuantity();
        valueQuantity.system = system;
        valueQuantity.unit = unit;
        valueQuantity.code = unit;
        valueQuantity.value =  unit.equalsIgnoreCase("Fahrenheit") ? modeOrMedian : Math.round(modeOrMedian);
        Log.d("Mono => (Fahrenheit if) =>", String.valueOf(valueQuantity.value));
        return valueQuantity;
    }

    private void unRegisterAndCloseConnection() {
        if(bluetoothGatt != null) {
            displayAndRegisterService(bluetoothGatt.getServices(), false);
            disconnectDeviceSelected();
        }
        stopScanning();
        returnToCallingApp(deviceDataList);
    }
}