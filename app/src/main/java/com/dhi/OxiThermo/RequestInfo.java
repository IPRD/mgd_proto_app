package com.dhi.OxiThermo;

import com.dhi.RequestResponse.Coding;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.ArrayList;
import java.util.HashMap;
/**
 *.
 * Date: Aug 30 2019
 * @author Apra Developers
 * @version 1.0
 */
@JsonIgnoreProperties
public class RequestInfo {

    public ArrayList<Test> tests = new ArrayList<>();
}

class Test{
    public String resourceType;
    public String id;
    public String status;
    public String intent;
    public Coding code;
    public HashMap<String, String> subject = new HashMap<>();
    public ArrayList<HashMap<String, String>> reasonCode = new ArrayList<HashMap<String, String>>();
}
