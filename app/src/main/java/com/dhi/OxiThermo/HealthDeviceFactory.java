package com.dhi.OxiThermo;
/**
 *
 * Date: Aug 30 2019
 * @author Apra Developers
 * @version 1.0
 */
public class HealthDeviceFactory{
    /**
     *
     * @param deviceType
     * @param data
     * @return
     */
    public HealthDevice getDevice(String deviceType, byte[] data){

        if("THERMOMETER".equalsIgnoreCase(deviceType)){
            return new ThermometerDevice(data);
        }else if("JUMPER".equalsIgnoreCase(deviceType)){
            return new SpO2Device(data);
        }

    return null;
    }

}
