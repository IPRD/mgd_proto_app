package com.dhi.OxiThermo;


import java.util.ArrayList;
/**
 *
 * Date: Aug 30 2019
 * @author Apra Developers
 * @version 1.0
 */
public class PulseOrTemparatureInfo {

    private String name = "";
    private Integer pulse;
    private Integer sp02;
    private Float pi;
    private Float temparature;
    private String result;

    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public Integer getSp02() {
        return sp02;
    }
    public void setSp02(Integer sp02) {
        this.sp02 = sp02;
    }

    public Integer getPulse() {
        return pulse;
    }
    public void setPulse(Integer pulse) {
        this.pulse = pulse;
    }

    public Float getPi() {
        return pi;
    }
    public void setPi(Float pi) {
        this.pi = pi;
    }

    public Float getTemparature() {
        return temparature;
    }
    public void setTemparature(Float temparature) {
        this.temparature = temparature;
    }

    public String getResult() {
        return this.result;
    }
    public void setResult(String result) {
        this.result = result;
    }

    /**
     *
     * For getting the list pulse result data
     * @param listOfPt : list of PulseOrTemparatureInfo objects
     * @return Float
     */
    public ArrayList<Float> getListOfPulse(ArrayList<PulseOrTemparatureInfo> listOfPt){

//        List<String> collect = listOfPt.stream()
//                .map(pt ->  pt.getPulse())
//                .collect(Collectors.toList());

        ArrayList<Float> listOfPulse = new ArrayList<>();
        for(PulseOrTemparatureInfo pt:listOfPt) {
            listOfPulse.add(Float.valueOf((pt.getPulse() == null ? 0 : pt.getPulse().floatValue())));
        }
        return listOfPulse;
    }

    /**
     *
     * @param listOfPt : list of PulseOrTemparatureInfo objects
     * @return : ArrayList<Float>
     */
//    public ArrayList<Float> getListOfPi(ArrayList<PulseOrTemparatureInfo> listOfPt){
//
//        ArrayList<Float> listOfPi = new ArrayList<>();
//        for(PulseOrTemparatureInfo pt:listOfPt) {
//            listOfPi.add(Float.valueOf((pt.getPi() == null ? 0 : pt.getPi().floatValue())));
//        }
//        return listOfPi;
//    }

    /**
     * For getting List Of Sp02 result objects
     * @param listOfPt :  list of PulseOrTemparatureInfo objects
     * @return : ArrayList<Float>
     */
    public ArrayList<Float> getListOfSp02(ArrayList<PulseOrTemparatureInfo> listOfPt){

        ArrayList<Float> listOfSp02 = new ArrayList<>();
        for(PulseOrTemparatureInfo pt:listOfPt) {
            listOfSp02.add(Float.valueOf((pt.getSp02() == null ? 0 : pt.getSp02().floatValue())));
        }
        return listOfSp02;
    }

    /**
     * getting list of temparature
     * @param listOfPt : ArrayList<Float>
     * @return : ArrayList<Float>
     */
//    public ArrayList<Float> getListOfTemparature(ArrayList<PulseOrTemparatureInfo> listOfPt){
//
//        ArrayList<Float> listOfTemp = new ArrayList<>();
//        for(PulseOrTemparatureInfo pt:listOfPt) {
//            listOfTemp.add(Float.valueOf((pt.getTemparature() == null ? 0 : pt.getTemparature().floatValue())));
//        }
//        return listOfTemp;
//    }
}