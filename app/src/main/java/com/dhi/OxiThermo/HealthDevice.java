package com.dhi.OxiThermo;

import java.util.ArrayList;
/**
 *.
 * Date: Aug 30 2019
 * @author Apra Developers
 * @version 1.0
 */
public interface HealthDevice {
    public PulseOrTemparatureInfo getDeviceResults();
}
