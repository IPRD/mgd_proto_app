package com.dhi.OxiThermo;

import android.util.Log;

import java.util.ArrayList;
/**
 *.
 * Date: Aug 30 2019
 * @author Apra Developers
 * @version 1.0
 */
public class ThermometerDevice implements HealthDevice {
    public byte[] data;

    public ThermometerDevice(byte[] data){
        this.data = data;
    }

    /**
     *  Set and get the thermometer result data
     * @return : PulseOrTemparatureInfo
     */
    public PulseOrTemparatureInfo getDeviceResults(){

            PulseOrTemparatureInfo pulseOrTemparatureInfo = new PulseOrTemparatureInfo();
            float temp;
            String result;
            boolean celsius = false;
            int i = data[2] & 0x80;
            if(i == 0) celsius=true;
            i = data[2] & 0x7F;
            i = i * 256;
            int k = data[3] & 0xFF;
            i = i + k;
            temp = i;
            temp = temp/100; //centigrate
            if(celsius) temp = 9*(temp/5)+32;
            result = "\n\n " + String.format("%5.1f", temp) + " " + "°F";
            if(temp > 0){

                pulseOrTemparatureInfo.setName("Temparature");
                pulseOrTemparatureInfo.setTemparature(temp);
                pulseOrTemparatureInfo.setResult(result);
            }
            Log.d("Temparature ",result);
        return pulseOrTemparatureInfo;
        }

}
