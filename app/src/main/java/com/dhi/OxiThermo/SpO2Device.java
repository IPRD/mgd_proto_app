package com.dhi.OxiThermo;

import java.util.ArrayList;
/**
 *.
 * Date: Aug 30 2019
 * @author Apra Developers
 * @version 1.0
 */
public class SpO2Device implements HealthDevice {

    private byte[] data;

    public SpO2Device(byte[] data){
        this.data = data;
    }

    /**
     * set and get the SPO2 results information.
     * @return : PulseOrTemparatureInfo
     */
    public PulseOrTemparatureInfo getDeviceResults(){

            PulseOrTemparatureInfo pulseOrTemparatureInfo = new PulseOrTemparatureInfo();
            String result = "";
            int pulserate = data[1]&0xFF;
            int sp02 = data[2]&0xFF;
            float pi = data[3]&0xFF;
            pi = pi/10;
            String p = String.format("%3d", pulserate);
            if((pulserate < 25)||(pulserate > 250)){
                p="---";
            }
            String sp = String.format("%3d", sp02);
            if((sp02 < 35)||(sp02 > 100)){
                sp="---";
            }
            String pis =  pi==0?"---":String.format("%3.1f", pi);
            result = "\n\n " + p + "  bpmPR\n" + " " + sp + "  %SpO2" ;

            if((!p.equals("---")) && (!sp.equals("---")) && (!pis.equals("---")) ){
                pulseOrTemparatureInfo.setName("Pulse");
                pulseOrTemparatureInfo.setPulse(Integer.parseInt(p.trim()));
                pulseOrTemparatureInfo.setSp02(Integer.parseInt(sp.trim()));
                pulseOrTemparatureInfo.setPi(pi);
                pulseOrTemparatureInfo.setResult(result);
            }
            return pulseOrTemparatureInfo;
        }
}
