package com.dhi.mgd;

import android.content.Context;
import android.content.SharedPreferences;

import com.dhi.RequestResponse.RequestDataModel;
/**
 *.
 * Date: Aug 30 2019
 * @author Apra Developers
 * @version 1.0
 */
public class LoginProperties {
    private String hName;
    private String pName;
    private String hGUID;
    private String pGUID;
    private String hDOB;
    private String pDOB;
    private int pHeartRate;
    private String hrUnit;
    private float pTemperature;
    private String tempUnit;
    private float pSPO2;
    private Context mCtx;
    private SharedPreferences mPref;
    private SharedPreferences.Editor mEdit;
    private static LoginProperties mInstance = null;
    private static RequestDataModel requestDataModel = null;
    private String pGender;


    private LoginProperties(Context ctx) {
        mCtx = ctx;
        mPref = ctx.getSharedPreferences("IdMSession",Context.MODE_PRIVATE);
        mEdit = mPref.edit();
        this.pName = "Patient";
        this.pDOB = "";
        this.pGUID = "";
        this.hGUID = "";
        this.pHeartRate = 0;
        this.pTemperature = 0;
        this.pSPO2 = 0;
        this.tempUnit = "C";
        this.hrUnit = "bpm";
    }

    public static void  setRequestDataModel(RequestDataModel requestDataModel){
        requestDataModel = requestDataModel;
    }

    public static RequestDataModel getRequestDataModel(){
        if(requestDataModel == null){
            return new RequestDataModel();
        }
        return requestDataModel;
    }

    /**
     * For getting Login Instance (LoginProperties) object.
     * @param ctx
     * @return
     */
    public static LoginProperties getLoginInstance(Context ctx) {
        if (mInstance == null)
            mInstance = new LoginProperties(ctx);
        return mInstance;
    }
    public String getpName() {
        String exValue = "";
        return (mPref.getString("PatientName", exValue));
    }

    public void setpName(String pName) {
        mEdit.putString("PatientName",pName);
        mEdit.commit();
     }

    /**
     * For getting the heart rate information.
     * @return : int
     */
    public int getpHeartRate() {
        String exValue = "";
        if (mPref.getString("PatientHeartRate", exValue) != null && mPref.getString("PatientHeartRate", exValue).length() > 0)
            return (Integer.parseInt(mPref.getString("PatientHeartRate", exValue)));
        else
            return 0;
    }

    /**
     * For set the given Patient Heart-Rate to SharedPreferences.Editor
     * @param pHeartRate
     */
    public void setpHeartRate(int pHeartRate) {
        mEdit.putString("PatientHeartRate",Integer.toString(pHeartRate));
        mEdit.commit();
    }

    public float getpTemperature() {
        String exValue = "";
        if (mPref.getString("PatientTemperature", exValue) != null && mPref.getString("PatientTemperature", exValue).length() > 0)
            return (Float.parseFloat(mPref.getString("PatientTemperature", exValue)));
        else
            return 0;
    }

    /**
     * For set the given temparature to SharedPreferences.Editor
     * @param pTemperature
     */
    public void setpTemperature(float pTemperature) {
        mEdit.putString("PatientTemperature",Float.toString(pTemperature));
        mEdit.commit();
    }

    public String getHName() {
        return hName;
    }

    public void setHName(String hName) {
        this.hName = hName;
    }

    public String getHGUID() {
        return hGUID;
    }

    public void setHGUID(String hGUID) {
        this.hGUID = hGUID;
    }

    public String getPGUID() {return pGUID; }

    public void setPGUID(String pGUID) {
        this.pGUID = pGUID;
    }

    public String getHDOB() {
        return hDOB;
    }

    public void setHDOB(String hDOB) {
        this.hDOB = hDOB;
    }

    public String getPDOB() {
        return pDOB;
    }

    public void setPDOB(String pDOB) {this.pDOB = pDOB;}

    public String getHrUnit() {
        return hrUnit;
    }

    public void setHrUnit(String hrUnit) {
        this.hrUnit = hrUnit;
    }

    public String getTempUnit() {
        return tempUnit;
    }

    public void setTempUnit(String tempUnit) {
        this.tempUnit = tempUnit;
    }

    public float getpSPO2() {
        return pSPO2;
    }

    public void setpSPO2(float pSPO2) {
        this.pSPO2 = pSPO2;
    }

    public String getPgender() {
        return pGender;
    }

    public void setpGender(String pGender) {
        this.pGender = pGender;
    }
}
