package com.dhi.mgd;

import com.dhi.RequestResponse.BasedOn;
import com.dhi.RequestResponse.Code;
import com.dhi.RequestResponse.Coding;
import com.dhi.RequestResponse.Entry;
import com.dhi.RequestResponse.ResourceObservation;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
//import static com.dhi.mgd.EmptyPlaceholders.HEARTRATE_CODE;
/**
 *.
 * Date: Aug 30 2019
 * @author Apra Developers
 * @version 1.0
 */

public class HealthCubeObservationMap {

    final String HEARTRATE_CODE = "8867-4";
    final String BODY_WEIGHT_CODE = "29463-7";
    final String BLOOD_PRESSURE_CODE = "85354-9";
    final String BLOOD_PRESSURE_SYSTOLIC = "8480-6";
    final String BLOOD__PRESSURE_DIASTOLIC = "8462-4";
    final String THINKMD_TEMPERATURE_CODE = "8310-5";
    final String THINKMD_SPO2_CODE = "20564-1";
    final String SPO2_CODE = "59408-5";
    final String HEIGHT_CODE = "8302-2";
    final String BLOOD_NEUTROPHILS_LEUKOCYTES_CODE = "26511-6";
    final String BLOOD_NEUTROPHILS_VOLUME_CODE = "26499-4";
    final String BLOOD_LYMPHOCYTES_CODE = "26478-8";
    final String BLOOD_LYMPHOCYTES_VOL_CODE = "26474-7";
    final String BLOOD_MONOCYTES_CODE = "26485-3";
    final String BLOOD_MONOCYTES_VOL_CODE = "26484-6";
    final String BLOOD_EOSINOPHILS_CODE = "26450-7";
    final String BLOOD_EOSINOPHILS_VOL_CODE = "26449-9";
    final String BLOOD_BASOPHILS_CODE = "30180-4";
    final String BLOOD_BASOPHILS_VOL_CODE = "26444-0";
    final String BLOOD_LEUKOCYTES_VOL_CODE = "26464-8";

    final String TYPHOID_CODE = "XTYPHOIDX";
    final String MALARIA_CODE = "76772-3";
    final String HAPATITIS_B_CODE = "75410-1";
    final String HAPATITIS_C_CODE = "89359-4";
    final String SYPHILIS_CODE = "13288-6";
    final String HIV_CODE = "80203-3";
    final String DENGUE_CODE = "75377-2";
    final String CHIKUNGUNYA_CODE = "XCHIKUNX";
    final String TROPONIN_CODE = "76399-5";
    final String D_DIMER_CODE = "XDDIMERX";
    final String CRP_CODE = "XCRPX";
    final String PROCALCITONINCODE = "XPROCALX";
    final String LEPTOSPIRA_CODE = "XLEPTOSX";
    final String PREGNANCY_COSE = "80384-1";
    final String URINE_PROTEIN_CODE = "20454-5";
    final String URINE_GLUCOSE_CODE = "25428-4";
    final String WCB_CODE = "XWBCX";
    final String ECG_CODE = "XECGX";
    final String HEMOCUE_CODE = "XHEMOCUEX";
    final String URINE_GLUCOSE_CODE1 = "25428-4  ";
    final String URINE_PROTEIN_CODE1 = "20454-5  ";
    final String URIC_ACID_CODE = "XURICACX";
    final String SYPHILIS_CODE1 = "13288-6 ";
    final String MID_ARM__CODE = "56072-2 ";
    final String HIV_CODE1 = "80203-3 ";
    final String HAPATITIS_B_CODE1 = "75410-1 ";
    final String HAPATITIS_C_CODE1 = "89359-4 ";
    final String HEMOGLOBIN_CODE = "XHEMOGLX";
    final String BLOOD_GROUPING_CODE = "882-1";
    final String BMI = "39156-5";
    final String CHOLESTEROL_CODE = "5932-9";
    final String BLOOD_GLUCOSE_RANDOM_CODE = "2339-0";
    final String BLOOD_GLUCOSE_NON_FASTING_CODE = "87422-2";
    final String BLOOD_GLUCOSE_FASTING_CODE = "1556-0";
    final String BPM_CODE = "8867-4";


    public HashMap<String, Entry> map;

    public HealthCubeObservationMap(){
        map = new HashMap<>();
        map.put(HEARTRATE_CODE,getEntry(HEARTRATE_CODE, "Heart rate","CarePlan 1","CarePlan/id1560954676957","http://loinc.org","preliminary","id1560954449058"));
        map.put(THINKMD_TEMPERATURE_CODE,getEntry(THINKMD_TEMPERATURE_CODE, "Temperature", "CarePlan 1", "CarePlan/id1560954676957", "http://loinc.org","preliminary","id1560954449058"));
        map.put(BODY_WEIGHT_CODE,getEntry(BODY_WEIGHT_CODE, "Body Weight", "CarePlan 1", "CarePlan/id1560954676957", "http://loinc.org","preliminary","id1560954449058"));
        map.put(THINKMD_SPO2_CODE,getEntry(THINKMD_SPO2_CODE, "Oxygen saturation","CarePlan 1", "CarePlan/id1560954676957", "http://loinc.org","preliminary","id1560954449058"));
        map.put(SPO2_CODE,getEntry(SPO2_CODE, "Oxygen saturation","CarePlan 1", "CarePlan/id1560954676957", "http://loinc.org","preliminary","id1560954449058"));
        map.put(BLOOD_PRESSURE_CODE,getEntry(BLOOD_PRESSURE_CODE, "blood pressure","CarePlan 1", "CarePlan/id1560954676957", "http://loinc.org","preliminary","id1560954449058"));
        map.put(BLOOD_PRESSURE_SYSTOLIC,getEntry(BLOOD_PRESSURE_SYSTOLIC, "Sysastolic blood pressure","CarePlan 1", "CarePlan/id1560954676957", "http://loinc.org","preliminary","id1560954449058"));
        map.put(BLOOD__PRESSURE_DIASTOLIC,getEntry(BLOOD__PRESSURE_DIASTOLIC, "Diastolic blood pressure","CarePlan 1", "CarePlan/id1560954676957", "http://loinc.org","preliminary","id1560954449058"));
        map.put(HEIGHT_CODE,getEntry(HEIGHT_CODE, "Height","CarePlan 1", "CarePlan/id1560954676957", "http://loinc.org","preliminary","id1560954449058"));

        map.put(BLOOD_NEUTROPHILS_LEUKOCYTES_CODE,getEntry(BLOOD_NEUTROPHILS_LEUKOCYTES_CODE, "Neutrophils/100 leukocytes in Blood by Automated count","CarePlan 1", "CarePlan/id1560954676957", "http://loinc.org","preliminary","id1560954449058"));
        map.put(BLOOD_NEUTROPHILS_VOLUME_CODE,getEntry(BLOOD_NEUTROPHILS_VOLUME_CODE, "Neutrophils [#/volume] in Blood","CarePlan 1", "CarePlan/id1560954676957", "http://loinc.org","preliminary","id1560954449058"));
        map.put(BLOOD_LYMPHOCYTES_CODE,getEntry(BLOOD_LYMPHOCYTES_CODE, "Lymphocytes/100 leukocytes in Blood by Automated count","CarePlan 1", "CarePlan/id1560954676957", "http://loinc.org","preliminary","id1560954449058"));
        map.put(BLOOD_LYMPHOCYTES_VOL_CODE,getEntry(BLOOD_LYMPHOCYTES_VOL_CODE, "Lymphocytes [#/volume] in Blood","CarePlan 1", "CarePlan/id1560954676957", "http://loinc.org","preliminary","id1560954449058"));
        map.put(BLOOD_MONOCYTES_CODE,getEntry(BLOOD_MONOCYTES_CODE, "Monocytes/100 leukocytes in Blood by Automated count","CarePlan 1", "CarePlan/id1560954676957", "http://loinc.org","preliminary","id1560954449058"));
        map.put(BLOOD_MONOCYTES_VOL_CODE,getEntry(BLOOD_MONOCYTES_VOL_CODE, "Monocytes [#/volume] in Blood","CarePlan 1", "CarePlan/id1560954676957", "http://loinc.org","preliminary","id1560954449058"));
        map.put(BLOOD_EOSINOPHILS_CODE,getEntry(BLOOD_EOSINOPHILS_CODE, "Eosinophils/100 leukocytes in Blood by Automated count","CarePlan 1", "CarePlan/id1560954676957", "http://loinc.org","preliminary","id1560954449058"));
        map.put(BLOOD_EOSINOPHILS_VOL_CODE,getEntry(BLOOD_EOSINOPHILS_VOL_CODE, "Eosinophils [#/volume] in Blood","CarePlan 1", "CarePlan/id1560954676957", "http://loinc.org","preliminary","id1560954449058"));
        map.put(BLOOD_BASOPHILS_CODE,getEntry(BLOOD_BASOPHILS_CODE, "Basophils/100 leukocytes in Blood by Automated count","CarePlan 1", "CarePlan/id1560954676957", "http://loinc.org","preliminary","id1560954449058"));
        map.put(BLOOD_BASOPHILS_VOL_CODE,getEntry(BLOOD_BASOPHILS_VOL_CODE, "Basophils [#/volume] in Blood","CarePlan 1", "CarePlan/id1560954676957", "http://loinc.org","preliminary","id1560954449058"));
        map.put(BLOOD_LEUKOCYTES_VOL_CODE,getEntry(BLOOD_LEUKOCYTES_VOL_CODE, "Leukocytes [#/volume] in Blood","CarePlan 1", "CarePlan/id1560954676957", "http://loinc.org","preliminary","id1560954449058"));

        map.put(TYPHOID_CODE,getEntry(TYPHOID_CODE, "Typhoid","CarePlan 1", "CarePlan/id1560954676957", "http://loinc.org","preliminary","id1560954449058"));
        map.put(MALARIA_CODE,getEntry(MALARIA_CODE, "MALARIA","CarePlan 1", "CarePlan/id1560954676957", "http://loinc.org","preliminary","id1560954449058"));
        map.put(HAPATITIS_B_CODE,getEntry(HAPATITIS_B_CODE, "Hepatitis B","CarePlan 1", "CarePlan/id1560954676957", "http://loinc.org","preliminary","id1560954449058"));
        map.put(HAPATITIS_C_CODE,getEntry(HAPATITIS_C_CODE, "Hepatitis C","CarePlan 1", "CarePlan/id1560954676957", "http://loinc.org","preliminary","id1560954449058"));
        map.put(SYPHILIS_CODE,getEntry(SYPHILIS_CODE, "Syphilis","CarePlan 1", "CarePlan/id1560954676957", "http://loinc.org","preliminary","id1560954449058"));
        map.put(HIV_CODE,getEntry(HIV_CODE, "HIV","CarePlan 1", "CarePlan/id1560954676957", "http://loinc.org","preliminary","id1560954449058"));
        map.put(DENGUE_CODE,getEntry(DENGUE_CODE, "Dengue","CarePlan 1", "CarePlan/id1560954676957", "http://loinc.org","preliminary","id1560954449058"));
        map.put(CHIKUNGUNYA_CODE,getEntry(CHIKUNGUNYA_CODE, "Chikungunya","CarePlan 1", "CarePlan/id1560954676957", "http://loinc.org","preliminary","id1560954449058"));
        map.put(TROPONIN_CODE,getEntry(TROPONIN_CODE, "Troponin","CarePlan 1", "CarePlan/id1560954676957", "http://loinc.org","preliminary","id1560954449058"));
        map.put(D_DIMER_CODE,getEntry(D_DIMER_CODE, "D DIMER","CarePlan 1", "CarePlan/id1560954676957", "http://loinc.org","preliminary","id1560954449058"));
        map.put(CRP_CODE,getEntry(CRP_CODE, "CRP","CarePlan 1", "CarePlan/id1560954676957", "http://loinc.org","preliminary","id1560954449058"));
        map.put(PROCALCITONINCODE,getEntry(PROCALCITONINCODE, "Procalcitonin","CarePlan 1", "CarePlan/id1560954676957", "http://loinc.org","preliminary","id1560954449058"));
        map.put(LEPTOSPIRA_CODE,getEntry(LEPTOSPIRA_CODE, "Leptospira","CarePlan 1", "CarePlan/id1560954676957", "http://loinc.org","preliminary","id1560954449058"));
        map.put(PREGNANCY_COSE,getEntry(PREGNANCY_COSE, "Pregnancy","CarePlan 1", "CarePlan/id1560954676957", "http://loinc.org","preliminary","id1560954449058"));
        map.put(URINE_PROTEIN_CODE,getEntry(URINE_PROTEIN_CODE, "Urine Protein","CarePlan 1", "CarePlan/id1560954676957", "http://loinc.org","preliminary","id1560954449058"));
        map.put(URINE_GLUCOSE_CODE,getEntry(URINE_GLUCOSE_CODE, "Urine Glucose","CarePlan 1", "CarePlan/id1560954676957", "http://loinc.org","preliminary","id1560954449058"));

    }

    private Entry getEntry( String codeCoing, String display, String displayBasedOn, String reference, String system, String status, String id) {
        ResourceObservation resourceObservation = new ResourceObservation();
        resourceObservation.id = id;
        resourceObservation.status = status;

        BasedOn bOn = new BasedOn();
        bOn.display = displayBasedOn;
        bOn.reference = reference;
        resourceObservation.basedOn.add(bOn);

        ArrayList<Coding> codeList = new ArrayList<>();

        Coding coding = new Coding(system,codeCoing,display);
//        coding.system = system;
//        coding.code = codeCoing;
//        coding.display = display;

        codeList.add(coding);
        Code code = new Code();
        code.coding = codeList;

        resourceObservation.code = code;
        resourceObservation.effectiveDateTime = new Date().toString();

        Entry e = new Entry();
        e.resource = resourceObservation;
        return e;
    }
}
