package com.dhi.mgd;
/**
 *.
 * Date: Aug 30 2019
 * @author Apra Developers
 * @version 1.0
 */
public enum PlaceholderState {
    HCW_DEVICES_NONE,
    HCW_DEVICES_MODULE,
    PATIENT_DEVICES_MEASUREMENT,
    PRIORS,
    MGD_PRIORS,
    TREATMENTS,
    HEALTH_STATS,
    ASSESMENTRESULT,
    NULL

}
