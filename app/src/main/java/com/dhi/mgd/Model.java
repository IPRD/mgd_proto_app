package com.dhi.mgd;
public class Model {
    /**
     *.
     * Date: Aug 30 2019
     * @author Apra Developers
     * @version 1.0
     */

    private boolean isSelected;
    private String deviceName;

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public boolean getSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}