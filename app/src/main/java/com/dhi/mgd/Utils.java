package com.dhi.mgd;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.util.Log;

import java.util.ArrayList;
/**
 *.
 * Date: Aug 30 2019
 * @author Apra Developers
 * @version 1.0
 */
public class Utils {
    private static String mQuestionStr, mAssesmentStr;
    private static ArrayList<Question> mQuestions;
    private static ArrayList<Question> mAssesments;
    static long prevts=0;
    private static Utils mInstance=null;
    public boolean CheckIfMessageTosend(){
        boolean ret = false;
        long millis = System.currentTimeMillis();
        long seconds = millis / 1000;
        if(seconds - prevts > 3){
            prevts = seconds;
            ret = true;
        }
        return ret;
    }

    /**
     *
     * @return
     */
    public static synchronized Utils GetUtils() {
        if (mInstance == null)
            mInstance = new Utils();
        return mInstance;
    }

    /**
     *
     * @return
     */
    public ArrayList<Question> getQuestionsList() {
        return mQuestions;
    }

    /**
     *
     * @param questionsList
     */
    public void setQuestionsList(ArrayList<Question> questionsList) {
        this.mQuestions = questionsList;
    }

    /**
     *
     * @param imgStr
     * @param outPic
     * @return
     */
    public static boolean getBmpFromString(String imgStr,Bitmap outPic) {
        //byte[] decodedString = Base64.decode(imgStr, Base64.DEFAULT);
        //byte[] decodedString = Base64.getDecoder.decode(imgStr);
        try {
            byte[] decodedString = java.util.Base64.getDecoder().decode(imgStr);
            outPic = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            return false;
        }
        return true;
        //return decodedByte;
    }

    /**
     *
     * @param ctx
     */
    public static void LoadQuestions(Context ctx) {
        mQuestionStr = JSONGenerator.loadJSONFromAsset(ctx, R.string.questions_json);
        if (mQuestionStr == null)
            Log.e("Error","Could not load questions");
    }

    /**
     *
     * @param s
     * @return
     */
    public static Question GetAssesment(String s){
        for (int i=0;i<mAssesments.size();i++){
            if(mAssesments.get(i).getGroupID().equalsIgnoreCase(s)) return mAssesments.get(i);
        }
        return null;
    }

    /**
     *
     * @return
     */
    public static ArrayList<Question>  FindQuestions() {
        ArrayList<Question> questions = new ArrayList<Question>();

        if (mQuestionStr != null && !mQuestionStr.isEmpty()) {
            {
                ArrayList<Question> q = JSONGenerator.getQuestions(mQuestionStr, "personal", "", "");
                questions.addAll(q);
            }
            for (int l = 1; l < 30; l++) {
                ArrayList<Question> q = JSONGenerator.getQuestions(mQuestionStr, Integer.toString(l), "", "");
                questions.addAll(q);
            }
        } else
            Log.d("Error","Questions str is null");

        return questions;
    }

    /**
     *
     * @param questions
     * @return
     */
    public static boolean  FindQuestions(ArrayList<Question> questions) {
        if (mQuestionStr != null && !mQuestionStr.isEmpty()) {
            {
                ArrayList<Question> q = JSONGenerator.getQuestions(mQuestionStr, "personal", "", "");
                questions.addAll(q);
            }
            for (int l = 1; l < 30; l++) {
                ArrayList<Question> q = JSONGenerator.getQuestions(mQuestionStr, Integer.toString(l), "", "");
                questions.addAll(q);
            }
            return true;
        } else
            Log.d("Error","Questions str is null");

        return false;
    }

    /**
     *
     * @param ctx
     */
    public static void LoadJsonForAssesment(Context ctx){
        LoadAssesment(ctx);
        mAssesments.clear();
        ArrayList<Question> assesment=new ArrayList<Question>();
        if(mAssesments.size()==0) {
            {
                ArrayList<Question> q = JSONGenerator.getAssesmentss(mAssesmentStr, "category");
                for (int k = 0; k < q.size(); k++) {
                    System.out.println(q.get(k).toString());
                }
                assesment.addAll(q);
            }            {
                ArrayList<Question> q = JSONGenerator.getAssesmentss(mAssesmentStr, "label");
                for (int k = 0; k < q.size(); k++) {
                    System.out.println(q.get(k).toString());
                }
                assesment.addAll(q);
            }            {
                ArrayList<Question> q = JSONGenerator.getAssesmentss(mAssesmentStr, "action");
                for (int k = 0; k < q.size(); k++) {
                    System.out.println(q.get(k).toString());
                }
                assesment.addAll(q);
            }            {
                ArrayList<Question> q = JSONGenerator.getAssesmentss(mAssesmentStr, "disease");
                for (int k = 0; k < q.size(); k++) {
                    System.out.println(q.get(k).toString());
                }
                assesment.addAll(q);
            }           {
                ArrayList<Question> q = JSONGenerator.getAssesmentss(mAssesmentStr, "treatment");
                for (int k = 0; k < q.size(); k++) {
                    System.out.println(q.get(k).toString());
                }
                assesment.addAll(q);
            }
            mAssesments = assesment;
        }
    }

    /**
     *
     * @param ctx
     */
    private static void LoadAssesment(Context ctx) {
        mAssesmentStr = JSONGenerator.loadJSONFromAsset(ctx, R.string.assesment_json);
    }

}
