package com.dhi.mgd;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.INTERNET;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.view.View.*;
/**
 *.
 * Date: Aug 30 2019
 * @author Apra Developers
 * @version 1.0
 */
public class MainActivity extends AppCompatActivity {
    private int CameraPermission = 1;
    private static final int PERMISSION_REQUEST_CODE = 200;
    private int HCW_TEST_CT = 2;
    private int PATIENT_TEST_CT = 3;
    private Context mCtx;
    private EditText hcwPatient;
    private EditText hcwPhone;
    private Spinner hcwCountry;
    private EditText hcwDOB;
    private CheckBox biometricChkBox;
    private boolean biometricAuthNeeded;
    Button nxtButton;
    Button loginButton;
    static boolean addeddata=false;

    //smsService mSmsSvc;
    Intent sendIntent;
    static boolean m_enableButton=false;
    final char CLOSING_BRACE = ')';
    final char PHONE_INDICATOR = '+';
    final String CONNECT = "ConnectToIdentity";
    final String ADD_PERSON = "AddPerson";
    static long prevts = 0;

    public void EnableLogin(boolean b){
        loginButton.setEnabled(b);
        loginButton.setVisibility(b?VISIBLE:INVISIBLE);
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
   //     tvResult.setText(intent.getStringExtra(ezdxPackageName + "RESULT"));  // result from Ezdx

   //     Log.d("--->>>", intent.getStringExtra(ezdxPackageName + "RESULT"));
        Toast.makeText(this, "OnNewIntent called", Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(!checkpermission()){
            requestPermission();
        }
        super.onCreate(savedInstanceState);

        biometricAuthNeeded = true;
        //mSmsSvc = new smsService();
        setContentView(R.layout.activity_hcwlogin);
        nxtButton = (Button) findViewById(R.id.hcwLoginNextBtn);
        loginButton = (Button) findViewById(R.id.hcwLoginBtn);

        EnableLogin(m_enableButton);
        final Calendar myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            /**
             *
             * @param view
             * @param year
             * @param monthOfYear
             * @param dayOfMonth
             */
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel(hcwDOB,myCalendar);
            }

        };

        mCtx = this.getApplicationContext();

        if( !addeddata) {
            Utils.LoadQuestions(mCtx);
            addeddata = true;
            Log.d("Success","Loaded questions");
       //     addTestData();
        }
        else
            Log.d("NA","Questions already present");


        ImageButton exitout = (ImageButton) findViewById(R.id.exithcwlogin);
        exitout.setOnClickListener(new OnClickListener() {
            /**
             *
             * @param view
             */
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        loginButton.setOnClickListener(new OnClickListener() {
            /**
             * Called when a view has been clicked.
             *
             * @param v The view that was clicked.
             */
            @Override
            public void onClick(View v) {
                Intent i=null;
                String name="test";
                String countryCode = "91";
                String phone = "98";
                String dob="tt";
                if((name.length()==0) && (phone.length() == 0) && (dob.length() == 0)) return;
                Log.d("HCW value",name);
                StringBuffer hcwguid=new StringBuffer();

                Map<String,String> searchParams = new HashMap<String,String>();
                if (name.length() != 0)
                    searchParams.put("name",name);
                if (phone.length() != 0)
                    searchParams.put("phone",phone);
                if (countryCode.length() != 0)
                    searchParams.put("countryCode",countryCode);
                if (dob.length() != 0)
                    searchParams.put("DOB",dob);
                searchParams.put("indType","HCW");
                StringBuffer missingFields = new StringBuffer();
                //   int userpreset = (IdentityModule.getInstance(mCtx)).isUserPresent(name,searchParams,hcwguid,missingFields);
                StringBuffer guid = new StringBuffer();

                String identityJSON = "{ \"resourceType\":\"IdentityRequest\", \"reqID\":\"101\", \"reqTxt\":\"AddPerson\", \"Inputs\": { \"currCtx\":\""+SessionProperties.getSessionInstance(mCtx).getSessionCtx()+"\",\"personType\":\"HCW\", \"skill\":\"\" } }";

                sendIntent.putExtra("IdentityRequest",identityJSON);
                sendIntent.setType("text/json");
                Intent chooser = Intent.createChooser(sendIntent, "Identity svc");
                if (sendIntent.resolveActivity(getPackageManager()) != null) {
                    if(Utils.GetUtils().CheckIfMessageTosend()) {
                        Log.d("idJSON",identityJSON);
                        startActivityForResult(chooser, 66);
                    //    biometricAuthNeeded = false;
                    }
                }
                }
        });

        nxtButton.setOnClickListener(new OnClickListener() {
            /**
             *
             * @param view
             */
            @Override
            public void onClick(View view) {

                Intent i = new Intent(
                        MainActivity.this,
                        EmptyPlaceholders.class);
                i.putExtras(getIntent());
                i.putExtra(getResources().getString(R.string.placehoder_enum), PlaceholderState.HCW_DEVICES_NONE);
                finish();
                startActivity(i);

            }
        });

        String identityJSON = "{ \"resourceType\":\"IdentityRequest\", \"reqID\":\"100\", \"reqTxt\":\"ConnectToIdentity\", \"Inputs\": { } }";
        sendIntent = new Intent();
        sendIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        sendIntent.setAction("org.iprd.identity.IdmService");
        sendIntent.putExtra("IdentityRequest",identityJSON);
        sendIntent.setType("text/json");
        Intent chooser = Intent.createChooser(sendIntent, "Identity svc");
        if (sendIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(chooser, 66);
        }

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode == RESULT_OK && requestCode == 66) {
            if (data.hasExtra("IdentityResponse")) {
                String t = data.getExtras().getString("IdentityResponse");
                try {
                    JSONObject tOut =new JSONObject(t);
                    String apiType = tOut.getString("reqTxt");
                    if (apiType.equals(CONNECT)) {
                        String currCtx = tOut.getJSONObject("Outputs").getString("currCtx");
                        String currAPI = tOut.getJSONObject("Outputs").getString("currAPI");
                        SessionProperties.getSessionInstance(mCtx).setSessionCtx(currCtx.toString());
                        String outStr = "Context: "+currCtx+"  API: "+currAPI;
                        Toast.makeText(this,outStr,
                                Toast.LENGTH_LONG).show();
                        m_enableButton = true;
                        EnableLogin(m_enableButton);
                    } else if (apiType.equals(ADD_PERSON)) {
                        String guid = tOut.getJSONObject("Outputs").getString("guid");
                        String dob = tOut.getJSONObject("Outputs").getString("dob");
                        String name = tOut.getJSONObject("Outputs").getString("name");
                        String success = tOut.getJSONObject("Outputs").getString("addDone");
                        if (success.length() > 0 && success.equals("true")) {
                            LoginProperties.getLoginInstance(mCtx).setHGUID(guid);
                            LoginProperties.getLoginInstance(mCtx).setHDOB(dob);
                            LoginProperties.getLoginInstance(mCtx).setHName(name);
                            nxtButton.callOnClick();
                        }
                        Toast.makeText(this,"Recvd addJSON result",Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(this,"Failure",
                            Toast.LENGTH_LONG).show();
                }
                //      Toast.makeText(this,t,
                //            Toast.LENGTH_LONG).show();
            }
            else
            {
                Toast.makeText(this,data.getDataString(),Toast.LENGTH_LONG).show();
            }
        } else if (resultCode == RESULT_CANCELED)
        {
            Toast.makeText(this,"Failure in API call. Please check the data you are sending",Toast.LENGTH_LONG).show();
        }
    }

    private void updateLabel(EditText edittext,Calendar myCalendar) {
        //String myFormat = "MM/dd/yy"; //In which you need put here
        String myFormat = "dd/MM/yyyy"; //date format used in Africa.
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        edittext.setText(sdf.format(myCalendar.getTime()));
    }


    private String extractDOBInLocalFormat(String dateUIFormat) {
        return (dateUIFormat.replace("/",""));
    }


    private String extractCountryCodeFromDropdown(String dropdownStr) {
        //loop thru string and extract number between + and )
        //ex: India(+91)
        String actualCountryCode = "";
        boolean numComing = false;
        for (int i=0;i<dropdownStr.length();i++) {
            char c = dropdownStr.charAt(i);
            if (numComing)
            {
                if (Character.isDigit(c))
                    actualCountryCode = actualCountryCode+Character.toString(c);
                else if (c == CLOSING_BRACE)
                {
                    numComing = false;
                    return actualCountryCode;
                }
                else
                {
                    //while parsing numbers, we should either encounter a numeric value
                    //or we should encounter a closing brace - indicating we're done reading the number
                    //anything other than that means this is possibly bad data so return null
                    return null;
                }
            }
            else if (c == PHONE_INDICATOR)
            {
                numComing = true;
            }
        }
        return null;
    }

    private void addTestData() {
        //if users already preent, no need to add.
        int hcwCt = 0;
        int patientCt = 0;


    }

    private void setButtonVisibility(int btnid, int visibility) {
        Button b = (Button) findViewById(btnid);
        b.setVisibility(visibility);
    }

    /**
     * onBackPressed() :  pop-up dialog for exit the MGD app or cancel the pop-up dialog box.
     */
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Exiting MGD")
                .setMessage("Are you sure you want to exit this application?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finishAndRemoveTask();
                        finish();
                    }

                })
                .setNegativeButton("No", null)
                .show();
    }

    private boolean checkpermission(){
        int res = ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA);
        int res1 = ContextCompat.checkSelfPermission(getApplicationContext(), WRITE_EXTERNAL_STORAGE);
        int res2 = ContextCompat.checkSelfPermission(getApplicationContext(), INTERNET);
        int res3 = ContextCompat.checkSelfPermission(getApplicationContext(), ACCESS_FINE_LOCATION);
        return res1 == PackageManager.PERMISSION_GRANTED && res == PackageManager.PERMISSION_GRANTED && res2 == PackageManager.PERMISSION_GRANTED && res3 == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission(){
        ActivityCompat.requestPermissions(this, new String[]{WRITE_EXTERNAL_STORAGE, CAMERA,ACCESS_FINE_LOCATION,INTERNET}, PERMISSION_REQUEST_CODE);
    }
}
