package com.dhi.mgd;

import android.util.Log;
import java.util.ArrayList;
import java.util.List;

import org.iprd.sharedutils.RestAPIHandler;

/**
 *.
 * Date: Aug 30 2019
 * @author Apra Developers
 * @version 1.0
 */
/*
this may be used to start a thinkMDService which uses a REST API to fetch content from thinkMD servers.
Right now, we are doing this using intent calls and this may not be required.
However, if needed in future, pls use this.
 */

public class thinkMDService {
    private RestAPIHandler mRestClient;

    public thinkMDService() {
        String remoteServer = "medsinc-v3-api-staging-pr-20.herokuapp.com";
        mRestClient = new RestAPIHandler(remoteServer);
    }

    /**
     * For getting thee assessments.
     * @param pType
     * @param audType
     * @param diseasesList
     * @param questions
     * @return
     */
    public String getAssessments(patientType pType,audienceType audType,List<String> diseasesList, String questions) {
        String assessmentsString = "/v3/assessment/score";
        String diseasesStr = "";
        //get diseases from diseases list as a comma separated list.
        for (int i=0;i<diseasesList.size();i++)
        {
            String disease = diseasesList.get(i);
            if (i == (diseasesList.size()-1))
                diseasesStr = diseasesStr+disease;
            else
                diseasesStr = diseasesStr+disease+",";
        }
        //build the URL required
        String completeAssessURL = assessmentsString+"?diseases="+diseasesStr+"&patient="+pType.toString()+"&audience="+audType.toString()+"&treatments=append";
        Log.d("Assesment sent",completeAssessURL);
        StringBuffer assessment = new StringBuffer();
        mRestClient.handlePostRequest(completeAssessURL,questions,assessment,true);
        return assessment.toString();
    }

    /**
     *
     */
    public void testAssessments() {
        patientType pType = patientType.child;
        audienceType audType = audienceType.chw;
        List<String> diseasesList = new ArrayList<String>();
        //respiratory_distress, dehydration, sepsis, malnutrition, anemia, measles, malaria, skin_infection, dysentery, meningitis, uti, ear_infection
        diseasesList.add("respiratory_distress");
        diseasesList.add("dehydration");
        diseasesList.add("sepsis");
        diseasesList.add("malnutrition");
        diseasesList.add("anemia");
        diseasesList.add("measles");
        diseasesList.add("malaria");
        diseasesList.add("skin_infection");
        diseasesList.add("dysentery");
        diseasesList.add("meningitis");
        diseasesList.add("uti");
        diseasesList.add("ear_infection");
        String questions = "[[{\"disclaimer.checked.selected\":true},{\"age.days.value\":1050.834},{\"gender.male.selected\":true},{\"why_is_the_child_being_seen.seizure_or_fit.selected\":true},{\"why_is_the_child_being_seen.cold_chills.selected\":true},{\"why_is_the_child_being_seen.foul_smelling_urine.selected\":true},{\"why_is_the_child_being_seen.pain_when_urinating.selected\":true},{\"why_is_the_child_being_seen.fever.selected\":true},{\"why_is_the_child_being_seen.bloody_stools.selected\":true},{\"why_is_the_child_being_seen.sick.selected\":true},{\"why_is_the_child_being_seen.ear_pain.selected\":true},{\"breathing.normal.selected\":true},{\"coughing.moderate.selected\":true},{\"vomiting_severity.moderate.selected\":true},{\"diarrhea.none.selected\":true},{\"drinking.much_less.selected\":true},{\"tears.no.selected\":true},{\"urinating.unknown.selected\":true},{\"sleeping.less.selected\":true},{\"weight.kg.value\":24},{\"temperature.celsius.value\":71},{\"heart_rate.bpm.value\":79},{\"respiratory_rate.bpm.value\":70},{\"oxygen_saturation.percent.value\":null},{\"muac.red.selected\":true},{\"disposition.irritable.selected\":true},{\"wakefulness.moderate.selected\":true},{\"skin_turgor.normal.selected\":true},{\"capillary_refill.normal.selected\":true},{\"latching.much_less.selected\":true},{\"nasal_flaring_or_retractions.yes.selected\":true},{\"chest_indrawing.unknown.selected\":true},{\"head_bobbing.unknown.selected\":true},{\"wheezing.no.selected\":true},{\"swelling_of_the_feet.unknown.selected\":true},{\"pale_eyelids.unknown.selected\":true},{\"pain_when_moving_neck.no.selected\":true},{\"skin_infection.discharge.selected\":true},{\"skin_infection.red.selected\":true},{\"skin_infection.warm.selected\":true},{\"nasal_discharge.yes.selected\":true},{\"red_eyes.unknown.selected\":true},{\"rash.no.selected\":true},{\"weight_percentile.percent.value\":93},{\"heart_rate_percentile.percent.value\":97},{\"respiratory_rate_percentile.percent.value\":32},{\"geo_location.lat.value\":null}]]";
        String assessment = getAssessments(pType,audType,diseasesList,questions);
        Log.d("Helo",assessment);
    }

}

