package com.dhi.mgd;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
/**
 *.
 * Date: Aug 30 2019
 * @author Apra Developers
 * @version 1.0
 */
public class Questionnaire extends AppCompatActivity {
    ArrayList<Question> mQuestions;
    int question_count = 0;
    Button mNxtBtn;
    TableLayout mTableLayout;
    RadioGroup.OnCheckedChangeListener rgListener;
    CheckBox.OnClickListener chkBoxListener;
    EditText inpField;
    TextView questionText, unitText;
    ImageButton prvButton, fwdButton;
    Spinner mSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questionnaire);
        questionText = (TextView) findViewById(R.id.questionText);
        questionText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20f);
        mQuestions = Utils.GetUtils().getQuestionsList();
        inpField = new EditText(this);
        unitText = new TextView(this);
        mSpinner = new Spinner(this);
        inpField.setWidth(200);
        inpField.setHeight(40);
        inpField.setMaxWidth(200);

        mTableLayout = (TableLayout) findViewById(R.id.questionOptions);
        mNxtBtn = (Button) findViewById(R.id.nxtBtnQuestionnaire);
        enableNextButton(false);

        ImageButton backBtn = (ImageButton) findViewById(R.id.backBtnQuestionnaire);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Questionnaire.this, EmptyPlaceholders.class);
                i.putExtras(getIntent());
                i.putExtra(getResources().getString(R.string.placehoder_enum), PlaceholderState.MGD_PRIORS);
                finish();
                startActivity(i);
            }
        });
        ImageButton login = (ImageButton) findViewById(R.id.hcwLgnQuestionnaire);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Questionnaire.this, MainActivity.class);
                finish();
                startActivity(i);
            }
        });

        ImageButton exitout = (ImageButton) findViewById(R.id.exitQuestionnaire);
        exitout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        ImageButton plogin = (ImageButton) findViewById(R.id.patientLgnQuestionnaire);
        plogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(Questionnaire.this, PatientEnroll.class);
                finish();
                startActivity(i);
            }
        });

        mNxtBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!SaveState()) {
                    return;
                }
                Intent i = new Intent(
                        Questionnaire.this,
                        EmptyPlaceholders.class);
                i.putExtras(getIntent());
                i.putExtra(getResources().getString(R.string.placehoder_enum), PlaceholderState.ASSESMENTRESULT);
                finish();
                startActivity(i);
            }
        });
        prvButton = (ImageButton) findViewById(R.id.backQuestion);
        rgListener = new RadioGroup.OnCheckedChangeListener() {
            /**
             *
             * @param group
             * @param checkedId
             */
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton checkedRadioButton = (RadioButton) group.findViewById(checkedId);
                // This puts the value (true/false) into the variable
                boolean isChecked = checkedRadioButton.isChecked();
                // If the radiobutton that has changed in check state is now checked...
                if (isChecked) {
                    String key = checkedRadioButton.getHint().toString();
                    Question quest;
                    try {
                        quest = mQuestions.get(question_count);
                        ArrayList<String> checkResp = new ArrayList<String>();
                        checkResp.add(key);
                        quest.setArrayResponse(checkResp);
                        mQuestions.set(question_count, quest);
                    } catch (IndexOutOfBoundsException e) {
                        e.printStackTrace();
                    }
                }
            }
        };

        chkBoxListener = new View.OnClickListener() {
            /**
             *
             * @param v
             */
            @Override
            public void onClick(View v) {
                CheckBox chkBox = (CheckBox) v;
                try {
                    Question quest = mQuestions.get(question_count);
                    ArrayList<String> options = quest.getArrayResponse();
                    if (chkBox.isChecked()) {
                        if (options.indexOf(chkBox.getHint().toString()) > -1) {
                            return;
                        }
                        options.add(chkBox.getHint().toString());
                    } else {
                        int idx = -1;
                        if ((idx = options.indexOf(chkBox.getHint().toString())) > -1) {
                            options.remove(idx);
                            return;
                        }
                    }
                    quest.setArrayResponse(options);
                    mQuestions.set(question_count, quest);
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
                //last page no need to say ff button
            }
        };

        prvButton.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param v
             */
            @Override
            public void onClick(View v) {
                ImageButton btn = (ImageButton) v;
                if (question_count == 0) {
                    return;
                }
                enableFwdButton(true);
                enableNextButton(false);
                SaveState();
                question_count--;
                enablePrvButton(question_count == 0 ? false : true);
                DisplayQuestion(Questionnaire.this);
            }
        });
        fwdButton = (ImageButton) findViewById(R.id.fwdQuestion);
        fwdButton.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param v
             */
            @Override
            public void onClick(View v) {
                ImageButton btn = (ImageButton) v;
                if (question_count == (mQuestions.size() - 1)) {
                    return;
                }
                enablePrvButton(true);
                if (!SaveState())
                    return;
                question_count++;
                boolean isLastQuestion = question_count == (mQuestions.size() - 1);
                enableFwdButton(!isLastQuestion);
                enableNextButton(isLastQuestion);
                DisplayQuestion(Questionnaire.this);
            }
        });
        DisplayQuestion(this);
        enablePrvButton(false);
    }

    private boolean SaveState() {
        Question quest;
        try {
            quest = mQuestions.get(question_count);
        } catch (IndexOutOfBoundsException e) {
            return false;
        }
        if (quest.getType() != QuestionType.BOOL && quest.getType() != QuestionType.MCQ) {
            try {
                String unit = quest.getOptions().size() > 1 ? mSpinner.getSelectedItem().toString() : unitText.getText().toString();
                for (Map.Entry<String, String> entry : quest.getOptions().entrySet()) {
                    if (entry.getValue().equals(unit)) {
                        unit = entry.getValue();
                        break;
                    }
                }
                if (quest.getType() == QuestionType.NUMBER) {
                    int num = Integer.parseInt(inpField.getText().toString());
                    quest.setNumResponse(unit, num);
                } else if (quest.getType() == QuestionType.REAL) {
                    double real = Double.parseDouble(inpField.getText().toString());
                    quest.setRealResponse(unit, real);
                } else if (quest.getType() == QuestionType.STRING) {
                    quest.setStringResponse(unit, inpField.getText().toString());
                }
            } catch (NumberFormatException e) {
                Toast t = Toast.makeText(Questionnaire.this, "Entered value is not valid", Toast.LENGTH_LONG);
                t.show();
                return false;
            }
            mQuestions.set(question_count, quest);
            return true;
        }
        if (quest.getArrayResponse().size() > 0)
            return true;

        Toast t = Toast.makeText(Questionnaire.this, "Atleast one option should be selected", Toast.LENGTH_LONG);
        t.show();
        return false;
    }

    private void DisplayQuestion(Context ctx) {
        int count = mTableLayout.getChildCount();
        for (int i = 0; i < count; i++) {
            View child = mTableLayout.getChildAt(i);
            if (child instanceof TableRow) ((ViewGroup) child).removeAllViews();
        }
        mTableLayout.removeAllViews();
        questionText.setText("");
        inpField.setText("");
        inpField.setTextSize(18f);
        unitText.setText("");
        Question quest;
        try {
            quest = mQuestions.get(question_count);
        } catch (IndexOutOfBoundsException e) {
            return;
        }
        questionText.setText(quest.getText());
        if (quest.getType() == QuestionType.MCQ) {
            ArrayList<TableRow> trArr = createCheckBoxes(quest.getOptions(), quest.getArrayResponse(), ctx);
            for (TableRow tr : trArr) {
                mTableLayout.addView(tr);
            }
            return;
        }
        if (quest.getType() == QuestionType.BOOL) {
            mTableLayout.addView(createRadioButtons(quest.getOptions(), quest.getArrayResponse().size() > 0 ? quest.getArrayResponse().get(0) : null, ctx));
            return;
        }
        if (quest.getType() == QuestionType.NUMBER) {
            inpField.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);
            inpField.setText("" + quest.getNumResponse());
            inpField.setSelectAllOnFocus(true);
        }
        if (quest.getType() == QuestionType.REAL) {
            inpField.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);
            inpField.setText("" + quest.getRealResponse());
            inpField.setSelectAllOnFocus(true);
        }
        if (quest.getType() == QuestionType.STRING) {
            inpField.setInputType(InputType.TYPE_TEXT_VARIATION_NORMAL);
            inpField.setText(quest.getStrResponse());
            inpField.setSelectAllOnFocus(true);
        }
        TableRow tr = new TableRow(ctx);
        tr.setOrientation(TableRow.HORIZONTAL);
        tr.addView(inpField);
        if (quest.getOptions().size() > 1) {
            addItemsToSpinner(quest.getOptions(), quest.getArrayResponse().size() > 0 ? quest.getArrayResponse().get(0) : null, ctx);
            tr.addView(mSpinner);
        } else if (quest.getOptions().size() == 1) {
            unitText.setText(quest.getOptions().entrySet().iterator().next().getValue());
            tr.addView(unitText);
        }
        mTableLayout.addView(tr);
    }

    TableRow createRadioButtons(Map<String, String> options, String activeKey, Context ctx) {
        TableRow tr = new TableRow(ctx);
        tr.setOrientation(TableRow.VERTICAL);
        RadioGroup parent = new RadioGroup(ctx);
        parent.setOrientation(RadioGroup.VERTICAL);
        for (Map.Entry<String, String> entry : options.entrySet()) {
            RadioButton radioButton = new RadioButton(ctx);
            radioButton.setText(entry.getValue());
            radioButton.setHint(entry.getKey());
            radioButton.setButtonTintList(ColorStateList.valueOf(Color.LTGRAY));
            parent.addView(radioButton);
            if (activeKey != null) {
                if (entry.getKey().equals(activeKey))
                    radioButton.setChecked(true);
            }
        }
        parent.setOnCheckedChangeListener(rgListener);
        tr.addView(parent);
        return tr;
    }

    ArrayList<TableRow> createCheckBoxes(Map<String, String> options, ArrayList<String> activeKeys, Context ctx) {
        ArrayList<TableRow> trArr = new ArrayList<TableRow>();
        for (Map.Entry<String, String> entry : options.entrySet()) {
            TableRow tr = new TableRow(ctx);
            tr.setOrientation(TableRow.VERTICAL);
            CheckBox chkBox = new CheckBox(ctx);
            chkBox.setText(entry.getValue());
            chkBox.setHint(entry.getKey());
            chkBox.setButtonTintList(ColorStateList.valueOf(Color.LTGRAY));
            if (activeKeys.indexOf(entry.getKey()) > -1)
                chkBox.setChecked(true);
            chkBox.setOnClickListener(chkBoxListener);
            tr.addView(chkBox);
            trArr.add(tr);
        }
        return trArr;
    }

    /**
     * Fior enabling the next button.
     * @param isEnabled
     */
    void enableNextButton(boolean isEnabled) {
        mNxtBtn.setBackground(getResources().getDrawable(isEnabled ? R.drawable.blue_rounded_background : R.drawable.grey_rounded_background, null));
        mNxtBtn.setEnabled(isEnabled);
    }

    /**
     *
     * For enable forward (questionnary forward) button
     * @param isEnabled
     */
    void enableFwdButton(boolean isEnabled) {
        fwdButton.setImageResource(isEnabled ? R.drawable.enabled_next_arrow : R.drawable.disabled_next_arrow);
        fwdButton.setEnabled(isEnabled);
    }

    /**
     * To enable previous button
     * @param isEnabled
     */
    void enablePrvButton(boolean isEnabled) {
        prvButton.setImageResource(isEnabled ? R.drawable.enabled_previous_arrow : R.drawable.disabled_previous_arrow);
        prvButton.setEnabled(isEnabled);
    }

    private void addItemsToSpinner(Map<String, String> itemMap, String activeStr, Context ctx) {
        List<String> itemsToDisplay = new ArrayList<String>();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(ctx, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        for (Map.Entry<String, String> entry : itemMap.entrySet()) {
            adapter.add(entry.getValue());
        }
        mSpinner.setAdapter(adapter);
        if (activeStr != null) {
            int idx = -1;
            if ((idx = adapter.getPosition(activeStr)) > -1) {
                mSpinner.setSelection(idx);
            }
        }
    }

    /**
     * onBackPressed() :  pop-up dialog for exit the MGD app or cancel the pop-up dialog box.
     */
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Exiting MGD")
                .setMessage("Are you sure you want to exit this application?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    /**
                     *
                     * @param dialog
                     * @param which
                     */
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finishAndRemoveTask();
                        finish();
                    }

                })
                .setNegativeButton("No", null)
                .show();
    }

}
