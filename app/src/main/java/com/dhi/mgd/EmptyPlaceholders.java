package com.dhi.mgd;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import com.dhi.OxiThermo.OximeterOrThermometer;
import android.widget.Toast;

import com.dhi.RequestResponse.BasedOn;
import com.dhi.RequestResponse.Code;
import com.dhi.RequestResponse.Coding;
import com.dhi.RequestResponse.Entry;
import com.dhi.RequestResponse.Identifier;
import com.dhi.RequestResponse.Individual;
import com.dhi.RequestResponse.Location;
import com.dhi.RequestResponse.Name;
import com.dhi.RequestResponse.PhysicalType;
import com.dhi.RequestResponse.RequestDataModel;
import com.dhi.RequestResponse.ResourceEncounter;
import com.dhi.RequestResponse.ResourceLocation;
import com.dhi.RequestResponse.ResourceObservation;
import com.dhi.RequestResponse.ResourcePatient;
import com.dhi.RequestResponse.ResourcePractitioner;
import com.dhi.RequestResponse.Subject;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.iprd.encdec.EncodeDecode;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.json.*;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 *.
 * Date: Aug 30 2019
 * @author Apra Developers
 * @version 1.0
 */

public class EmptyPlaceholders extends AppCompatActivity {

    TextView m_plcTxtView = null;
    TextView m_AssesmentResult =null;
    TextView m_HeartRateResult =null;
    PlaceholderState m_state;
    String m_msg = "";
    Button m_mgdMalaria,m_nxtButton,m_newPatientButton;
    Context mCtx;

    final static String hearrateapp="com.tzutalin.dlibtest";
    final static String respirationrateapp="com.dhi.mgd";
    final char OPENING_BRACE = '{';
    final char CLOSING_BRACE = '}';
    final String SUBJECT_NAME = "$SUBJECT";
    final static String HEARTRATE_CODE = "8867-4";
    final String SPO2_CODE = "59408-5";
    private Button preferrenceSettingBtnMgd;
    final String THINKMD_TEMPERATURE_CODE = "8310-5";
    TextView tvResult;

    private LinearLayout linearLayout4deviceList;

    String patientDataInHl7String = "{\"identifier\":[{\"system\":\"iprd\",\"use\":\"usual\",\"value\":\"patientid123\"}],\"resourceType\":\"Patient\",\"address\":[{\"country\":\"india\",\"type\":\"physical\",\"use\":\"home\"}],\"birthDate\":\"2004-06-24\",\"gender\":\"male\",\"name\":[{\"given\":[\"john doe\"]}],\"telecom\":[{\"rank\":1,\"system\":\"phone\",\"use\":\"mobile\",\"value\":\"9999887776\"}]}";

    String hightlightTests = "[\"PULSE_OXIMETER\",\"WEIGHT\",\"BLOOD_PRESSURE\",\"CHOLESTEROL\",\"CHIKUNGUNYA\",\"SYPHILIS\"]";
    public static RequestDataModel requestDataModel = new RequestDataModel();
    private Boolean isTrmpObjAdded = false, isHeartRateObjAdded = false, isSpo2ObjAdded = false;
    public String requestDataModelString;

    String sampleUserId = "+911111133333";
    String secretKey = "123";
    //public static final String ezdxPackageName = "com.healthcubed.ezdx.ezdx.dev";  // pointing to development environment, For your testing
    public static final String ezdxPackageName = "com.healthcubed.ezdx.ezdx.demo";

    private ListView mListView;
    private ArrayList<Model> modelArrayList;
    public ArrayList<Model> mAl = null;
    private CustomAdapter customAdapter;
    private String[] availableDevices = { "HEART RATE", "PULSE AND SPO2", "TEMPERATURE", "HEALTHCUBE "};

    /**
     * Loads json data from assets
     * @param context : Context
     * @param fname : file name
     * @return : String
     */
    public static String loadJSONFromAsset(Context context,String fname) {
        String json = null;
        if (context == null) {
            throw new IllegalArgumentException("Context unavailable");
        } else {
            Resources res = context.getResources();

            try {
                InputStream is = res.getAssets().open(fname);
                int size = is.available();
                if (size == 0) {
                    return null;
                } else {
                    byte[] buffer = new byte[size];
                    is.read(buffer);
                    is.close();
                    json = new String(buffer, "UTF-8");
                    return json;
                }
            } catch (IOException var7) {
                var7.printStackTrace();
                return null;
            }
        }
    }


    private class AsyncTaskRunner extends AsyncTask<String, String ,String> {

        private String resp;
        @Override
        protected String doInBackground(String... params) {
            publishProgress("\nPlease wait ... \nConnecting to server for fetching Assessment.... \n "); // Calls onProgressUpdate()
            try {
                String tobesent="";
                List<String> l = new ArrayList<String>();
                thinkMDService t = new thinkMDService();
                if (params.length > 1) {
                    l.add(params[0]);
                    l.add("respiratory_distress");
                    l.add("dehydration");
                    l.add("sepsis");
                    l.add("malnutrition");
                    l.add("anemia");
                    l.add("measles");
                    l.add("malaria");
                    l.add("skin_infection");
                    l.add("dysentery");
                    l.add("meningitis");
                    l.add("uti");
                    l.add("ear_infection");
                    tobesent=params[1];
                }
                String ret = t.getAssessments(patientType.child, audienceType.chw, l, "[" + tobesent + "]");
                Log.d("Assesment received",ret);
                resp="";
                if(ret.length()>0){
                    Utils.LoadJsonForAssesment(getApplicationContext());
                    //m_plcTxtView.setText("Calling:Generate Treatment Plan Module");
                    resp = "\n";
                    ArrayList<AssessmentScore> parseResp = JSONGenerator.parseAssessmentResponse(ret);
                    Question f = Utils.GetAssesment("category");
                    Question a = Utils.GetAssesment("action");
                    Question tr = Utils.GetAssesment("treatment")  ;
                    Question d = Utils.GetAssesment("disease");
                    Question la = Utils.GetAssesment("label");

                    if (parseResp == null)
                    {
                        Log.e("ThinkMD error","Bad data recvd from thinkMD service");
                        return "\n\n\n Invalid data received \n\n\n";
                    }
                    for(int j=0;j<parseResp.size();j++) {
                        if(parseResp.get(j).Severity.equalsIgnoreCase("none"))continue;
                        String val = d.getOptions().get(parseResp.get(j).Disease);
                        resp += val + ":: \n";
                        val = la.getOptions().get(parseResp.get(j).Severity);
                        resp += val + ".\n";

                        ArrayList<TreatmentResult> treatmentResults = parseResp.get(j).getTreatment();
                        String followup="";
                        String tremnt="";
                        for(int k=0;k<treatmentResults.size();k++){
                            if(treatmentResults.get(k).Catagory.equalsIgnoreCase("followup")){
                                followup += a.getOptions().get(treatmentResults.get(k).Name)+ "\n";
                            }
                            if(treatmentResults.get(k).Catagory.equalsIgnoreCase("treatment")){
                                String s = tr.getOptions().get(treatmentResults.get(k).Name);
                                if(s==null) s= a.getOptions().get(treatmentResults.get(k).Name);
                                tremnt += s+ "\n";
                            }
                            //resp += tre.get(k).Name + "--" + tre.get(k).Catagory + "\n" ;
                        }
                        String tmp = "";
                        tmp = followup.length() >0 ? f.getOptions().get("followup")+": \n" :"";
                        followup = tmp + followup;
                        tmp = tremnt.length() >0 ? f.getOptions().get("treatment")+": \n" :"";
                        tremnt = tmp + tremnt;
                        resp += followup + tremnt;
                        resp += "\n----------------------------------------------------------------------------\n";
                    }
                }else{
                    resp = "\n\n\nCould not receive any Assesment \n\n\n";
                }

            } catch (Exception e) {
                e.printStackTrace();
                Log.e("thinkMD error",e.getMessage());
                resp = "\n\n\n Invalid data received \n\n\n";
            }
            //publishProgress(resp);
            return resp;
        }

        @Override
        protected void onPostExecute(String result) {
            // execution of result of Long time consuming operation
            m_nxtButton.setBackground(getResources().getDrawable(R.drawable.blue_rounded_background,null));
            m_nxtButton.setEnabled(true);

            m_AssesmentResult.setText(result);
            m_AssesmentResult.setMovementMethod(new ScrollingMovementMethod());
        }

        @Override
        protected void onPreExecute() {
            m_AssesmentResult.setText("Waiting for Assesment Result....");
        }

        @Override
        protected void onProgressUpdate(String... text) {
            m_AssesmentResult.setText(text[0]);
            m_AssesmentResult.setMovementMethod(new ScrollingMovementMethod());
        }
    }

    /**
     * For checking intant availability.
     * @param app : Application package name(String)
     * @return : boolean
     */
    boolean checkIntentAvailability(String app){
        boolean ret = false;
        Intent launcherIntent = getPackageManager().getLaunchIntentForPackage(app);
        if(launcherIntent != null){
            ret = true;
        }
        return ret;
    }

    /**
     * onBackPressed() :  pop-up dialog for exit the MGD app or cancel the pop-up dialog box.
     */
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
        .setIcon(android.R.drawable.ic_dialog_alert)
        .setTitle("Exiting MGD")
        .setMessage("Are you sure you want to exit this application?")
        .setPositiveButton("Yes", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finishAndRemoveTask();
                finish();
            }

        })
        .setNegativeButton("No", null)
        .show();
    }
    int REQUEST_CODE =66;
    int REQUEST_CODE1 =12345;


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("==<<=OnActRes=",(data != null) ? data.toString() : "NULL DATA");
        if (resultCode == RESULT_CANCELED){
            Toast.makeText(this,"Failure in API call. Please check the data you are sending",Toast.LENGTH_LONG).show();
            return;
        }
        if (resultCode == RESULT_OK && requestCode == REQUEST_CODE) {
            if (data.hasExtra("observationResponse")) {
                String t = data.getExtras().getString("observationResponse");
                try {
                    JSONObject tOut =new JSONObject(t);
                    String outBPM1 = tOut.getJSONObject("valueQuantity").getString("value");
                    float bpm = Float.parseFloat(outBPM1);
                    String outBPM = Float.toString(Math.round(bpm));
                    String outDate = tOut.getString("effectiveDateTime");
                    String outStr = " \n" + "Heart rate received from Video Vitals : " + outBPM+" BPM"+"\n";
                    m_HeartRateResult.setText(outStr);
                    m_HeartRateResult.setVisibility(View.VISIBLE);

                    Toast.makeText(this,outStr,Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(this,"Failure",Toast.LENGTH_LONG).show();
                }
            } else if (data.hasExtra("IdentityResponse")) {
                String t = data.getExtras().getString("IdentityResponse");
                try {
                    JSONObject tOut =new JSONObject(t);
                    String apiType = tOut.getString("reqTxt");
                    if (apiType.equals("POS")) {
                        String posDone = tOut.getJSONObject("Outputs").getString("posDone");
                        if (posDone.equals("true")) {
                            LoginProperties.getLoginInstance(getApplicationContext()).setPGUID("");
                            setButtonVisibility(R.id.plhdNextPatientBtn,View.VISIBLE);
                            setButtonVisibility(R.id.plhdNextBtn,View.INVISIBLE);
                            m_newPatientButton.callOnClick();
                        }
                        Toast.makeText(this,"Recvd addJSON result",Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(this,"Failure",Toast.LENGTH_LONG).show();
                }
            }
        }
        if (resultCode == RESULT_OK && requestCode == REQUEST_CODE1) {
            if (data.hasExtra("observationResponse")) {

                String ret = "";
                try {
                    JSONObject jsonObj = new JSONObject(data.getExtras().getString("observationResponse"));
                    JSONArray arr = jsonObj.getJSONArray("entry");
                    String outval = "";
                    for(int i=0; i < arr.length(); i++){
                        JSONObject ob = (JSONObject)arr.get(i);
                        String svcLOINCCode = ob.getJSONObject("resource").getJSONObject("code").getJSONArray("coding").getJSONObject(0).getString("code");
                        Log.d("svcLOINCCode----->",svcLOINCCode+"");
                        String val = ob.getJSONObject("resource").getJSONObject("valueQuantity").getString("value");//ob.getJSONObject("valueQuantity").getString("value");
                        String unit = ob.getJSONObject("resource").getJSONObject("valueQuantity").getString("unit");
                        if (unit.equals("beats/minute"))
                            unit = "bpm";
                        if (svcLOINCCode.equals(HEARTRATE_CODE)){
                            float bpm = Float.parseFloat(val);
                            int bp=((int)(bpm+0.5));
                            LoginProperties.getLoginInstance(getApplicationContext()).setpHeartRate(bp);
                            LoginProperties.getLoginInstance(getApplicationContext()).setHrUnit(unit);
                            String outBPM = Integer.toString(bp);
                            outval = outval + "\n" + "Heart rate: " + outBPM+" " + unit;
                            setBiometriData(val, unit, HEARTRATE_CODE,"http://unitsofmeasure.org");
                        } else if (svcLOINCCode.equals(SPO2_CODE)){
                            Log.d("svcLOINCCode----->",svcLOINCCode);
                            float spo2 = Float.parseFloat(val);
                            LoginProperties.getLoginInstance(getApplicationContext()).setpSPO2(spo2);
                            int bp=((int)(spo2));
                            String outSPO2 =  Integer.toString(bp);
                            outval = outval + "\n" + "SPO2: " + outSPO2 + " "+unit;
                            Log.d("spo2 ---",val);
                            setBiometriData(val, "%", SPO2_CODE,"http://unitsofmeasure.org");
                        }  else if (svcLOINCCode.equals(THINKMD_TEMPERATURE_CODE)){
                            float temp = Float.parseFloat(val);
                            //float temprtr = 0.0f;
                            LoginProperties.getLoginInstance(getApplicationContext()).setpTemperature(temp);
                            LoginProperties.getLoginInstance(getApplicationContext()).setTempUnit(unit);
                            Log.d("mono", String.valueOf(temp));

                            String tempr = String.format("%5.1f", temp);
                            Log.d("madhav",tempr);
                            outval = outval + "\n" + "Temperature : " + tempr + " "+unit;
                            String tempStr = String.format("%5.1f", temp);
                            Log.d("madhav",tempStr+"");

                            setBiometriData(Float.toString(temp), demoSetUnit(unit), THINKMD_TEMPERATURE_CODE,"http://unitsofmeasure.org");//
                        }
                    }
                    m_HeartRateResult.setText(outval);
                    m_HeartRateResult.setVisibility(View.VISIBLE);

                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(this,"Failure",Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    private void setBiometriData(String val, String unit, String code, String system) {
    Log.d("vallll", val);
        ResourceObservation resourceObservationResHeart = new ResourceObservation();
        for (Entry ent : requestDataModel.entry) {
            Log.d("setBiometriData type cast :",ent.resource.getClass().getSimpleName().equalsIgnoreCase("ResourceObservation")+"");
            if(ent.resource.getClass().getSimpleName().equalsIgnoreCase("ResourceObservation")) {
                resourceObservationResHeart = (ResourceObservation) ent.resource;
                if (resourceObservationResHeart.resourceType.equalsIgnoreCase("Observation") && resourceObservationResHeart.code.coding.get(0).code.equalsIgnoreCase(code)) {
                    resourceObservationResHeart.valueQuantity.value = Float.parseFloat((val == null) ? "0.0f" : val);
                    resourceObservationResHeart.valueQuantity.unit = unit;
                    resourceObservationResHeart.valueQuantity.code = code;
                    resourceObservationResHeart.valueQuantity.system = system;
                    break;
                }
            }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCtx = getApplicationContext();
        m_state = PlaceholderState.NULL;
        setContentView(R.layout.activity_empty_placeholders);
        m_plcTxtView = (TextView) findViewById(R.id.plhdLbl);
        m_AssesmentResult = (TextView) findViewById(R.id.Assesmentplaceholder);
        m_AssesmentResult.setTextSize(16f);
        m_HeartRateResult =  (TextView) findViewById(R.id.HeartRateValue);
        //m_AssesmentResult.setMovementMethod(new ScrollingMovementMethod());
        m_nxtButton = (Button) findViewById(R.id.plhdNextBtn);
        m_newPatientButton = (Button) findViewById(R.id.plhdNextPatientBtn);
        linearLayout4deviceList = (LinearLayout) findViewById(R.id.linearLayoutForDevice);//???
        tvResult = findViewById(R.id.tv_result);
        final Context localCtx = getApplicationContext();

        preferrenceSettingBtnMgd = (Button) findViewById(R.id.preferrenceSettingBtnMgd);
        preferrenceSettingBtnMgd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(EmptyPlaceholders.this, MyPreferencesActivity.class);
                startActivity(i);
            }
        });

        m_nxtButton.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param view
             */
            @Override
            public void onClick(View view) {
                boolean is1stIter = true;
                Class<? extends AppCompatActivity> destClass = null;
                switch (m_state) {
                    case HCW_DEVICES_NONE:
                        Button button = (Button) findViewById(R.id.noneBtn);
                        button.setVisibility(View.INVISIBLE);
                        setImageButtonVisibility(R.id.patientLgnPlhd, View.INVISIBLE);
                        destClass = EmptyPlaceholders.class;
                        createIntent(destClass, PlaceholderState.HCW_DEVICES_MODULE);
                        break;

                    case HCW_DEVICES_MODULE:
                        button = (Button) findViewById(R.id.heartRateBtn);
                        button.setVisibility(View.INVISIBLE);
                        button = (Button) findViewById(R.id.respirationBtn);
                        button.setVisibility(View.INVISIBLE);
                        button = (Button) findViewById(R.id.temperatureBtn);
                        button.setVisibility(View.INVISIBLE);
                        button = (Button) findViewById(R.id.healthCubeBtn);
                        button.setVisibility(View.INVISIBLE);
                        linearLayout4deviceList.setVisibility(View.INVISIBLE);
                        setImageButtonVisibility(R.id.patientLgnPlhd, View.INVISIBLE);
                        destClass = PatientEnroll.class;
                        createIntent(destClass, PlaceholderState.NULL);
                        break;
                    case PATIENT_DEVICES_MEASUREMENT:
                        m_HeartRateResult.setVisibility(View.INVISIBLE);
                        button = (Button) findViewById(R.id.heartRateBtn);
                        button.setVisibility(View.INVISIBLE);
                        button = (Button) findViewById(R.id.respirationBtn);
                        button.setVisibility(View.INVISIBLE);
                        button = (Button) findViewById(R.id.temperatureBtn);
                        button.setVisibility(View.INVISIBLE);
                        button = (Button) findViewById(R.id.healthCubeBtn);
                        button.setVisibility(View.INVISIBLE);
                        linearLayout4deviceList.setVisibility(View.INVISIBLE);
                        setImageButtonVisibility(R.id.patientLgnPlhd, View.INVISIBLE);
                        destClass = EmptyPlaceholders.class;
                        createIntent(destClass, PlaceholderState.PRIORS);
                        break;

                    case PRIORS:
                        setSpinnerVisibility(R.id.seasonalPrior, View.INVISIBLE);
                        setSpinnerVisibility(R.id.nearbyPrior, View.INVISIBLE);
                        setSpinnerVisibility(R.id.patientPrior, View.INVISIBLE);
                        setTextVisibility(R.id.txtseasonalprior, View.INVISIBLE);
                        setTextVisibility(R.id.txtnearbyprior, View.INVISIBLE);
                        setTextVisibility(R.id.txtpatientprior, View.INVISIBLE);
                        setImageButtonVisibility(R.id.patientLgnPlhd, View.INVISIBLE);
                        destClass = EmptyPlaceholders.class;
                        createIntent(destClass, PlaceholderState.MGD_PRIORS);
                        break;
                    case MGD_PRIORS:
                        setImageButtonVisibility(R.id.patientLgnPlhd, View.INVISIBLE);
                        destClass = EmptyPlaceholders.class;
                        createIntent(destClass, PlaceholderState.ASSESMENTRESULT);
                        break;
                    case TREATMENTS:
                        //   setSpinnerVisibility(R.id.treatmentLocation, View.INVISIBLE);
                        //  setTextVisibility(R.id.txtlocprior, View.INVISIBLE);
                        //  setImageButtonVisibility(R.id.patientLgnPlhd, View.INVISIBLE);
                        String identityJSON = "{ \"resourceType\":\"IdentityRequest\", \"reqID\":\"102\", \"reqTxt\":\"HandlePOS\", \"Inputs\": { \"currCtx\":\""+SessionProperties.getSessionInstance(mCtx).getSessionCtx()+"\"} }";
                       // \"Inputs\": { \"currCtx\":\""+SessionProperties.getSessionInstance(mCtx).getSessionCtx()+"\"

                        Intent sendIntent = new Intent();
                        sendIntent.setAction("org.iprd.identity.IdmService");
                        sendIntent.putExtra("IdentityRequest",identityJSON);
                        sendIntent.setType("text/json");
                        Intent chooser = Intent.createChooser(sendIntent, "Identity svc");
                        if (sendIntent.resolveActivity(getPackageManager()) != null) {
                            if(Utils.GetUtils().CheckIfMessageTosend()) {
                                Log.d("idJSON", identityJSON);
                                startActivityForResult(chooser, 66);
                            }
                        }
                        //     setButtonVisibility(R.id.plhdNextPatientBtn,View.VISIBLE);
                        //    createIntent(destClass, PlaceholderState.NULL);
                        break;
                    case ASSESMENTRESULT:
                        m_AssesmentResult.setVisibility(View.INVISIBLE);
                        m_AssesmentResult.setText("");
                        destClass = EmptyPlaceholders.class;
                        createIntent(destClass, PlaceholderState.TREATMENTS);
                        setImageButtonVisibility(R.id.patientLgnPlhd, View.INVISIBLE);
                        break;
                    case HEALTH_STATS:
                        destClass = PatientEnroll.class;
                        break;
                    default:
                        destClass = PatientEnroll.class;
                }
            }
        });

        ImageButton login = (ImageButton) findViewById(R.id.hcwLgnPlhd);

        login.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param view
             */
            @Override
            public void onClick(View view) {
                Intent i = new Intent(EmptyPlaceholders.this, MainActivity.class);
                finish();
                startActivity(i);
            }
        });
/**
 *
 */
        ImageButton exitout = (ImageButton) findViewById(R.id.exitPlhd);
        exitout.setOnClickListener(new View.OnClickListener() {
            /**
             *
              * @param view
             */
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        ImageButton plogin = (ImageButton) findViewById(R.id.patientLgnPlhd);
        plogin.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param view
             */
            @Override
            public void onClick(View view) {
                Intent i = new Intent(EmptyPlaceholders.this, PatientEnroll.class);
                finish();
                startActivity(i);
            }
        });


        Button pheartrate = (Button) findViewById(R.id.heartRateBtn);
        pheartrate.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param view
             */
            @Override
            public void onClick(View view) {
                //if (m_state == PlaceholderState.HCW_DEVICES_MODULE) return;
                String title = "Searching for specific Heartrate intent";
               // String svcReqJSON = "{\"tests\":[{\"resourceType\":\"ServiceRequest\",\"id\":\"example_request_HR\",\"status\":\"active\",\"intent\":\"original-order\",\"code\":{\"coding\":[{\"system\":\"http://loinc.org\",\"code\":\"8867-4\",\"display\":\"Heart rate\"}],\"text\":\"Heart Rate\"},\"subject\":{\"reference\":\"patient_example\"},\"reasonCode\":[{\"text\":\"Reason as to why, this can be omitted also\"}]}";

                String svcReqJSON = "{\"entry\":[{\"fullUrl\":\"http://iprdgroup.com/FHIR/Resources\",\"resource\":{\"resourceType\":\"ServiceRequest\",\"id\":\"example_request_HR\",\"status\":\"active\",\"intent\":\"original-order\",\"code\":{\"coding\":[{\"system\":\"http://loinc.org\",\"code\":\"8867-4\",\"display\":\"Heart rate\"}],\"text\":\"Heart rate\"},\"subject\":{\"reference\":\"Patient/patient_example\"}}}],\"resourceType\":\"Bundle\",\"type\":\"collection\"}";

                isHeartRateObjAdded = isObjectAdded(HEARTRATE_CODE);

                if(!isHeartRateObjAdded) {
                    Entry e = getEntry(HEARTRATE_CODE, "Heart rate","CarePlan 1", "CarePlan/id1560954676957", "http://loinc.org");
                    requestDataModel.entry.add(e);
                }
                Intent sendIntent = new Intent();
                //sendIntent.setAction("edu.psu.videovitals.heartrate");
                sendIntent.setAction(hearrateapp);
                sendIntent.putExtra("serviceRequest",svcReqJSON);
                sendIntent.setType("text/json");
                Intent chooser = Intent.createChooser(sendIntent, title);
                if (sendIntent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(chooser, REQUEST_CODE1);
                }
            }
        });

        Button pPulseSPO2 = (Button) findViewById(R.id.respirationBtn);
        pPulseSPO2.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param v
             */
            @Override
            public void onClick(View v) {

                //if (m_state == PlaceholderState.HCW_DEVICES_MODULE) return;
               String svcReqJSON =
                        "{\"entry\":[{\"fullUrl\":\"http://iprdgroup.com/FHIR/Resources\",\"resource\":{\"resourceType\":\"ServiceRequest\",\"id\":\"example_request_HR\"," +
                                "\"status\":\"active\",\"intent\":\"original-order\",\"code\":{\"coding\":[{\"system\":\"http://loinc.org\",\"code\":\"8867-4\"," +
                                "\"display\":\"Heart rate\"}],\"text\":\"Heart rate\"},\"subject\":{\"reference\":\"Patient/patient_example\"}}}," +
                                "{\"fullUrl\":\"http://iprdgroup.com/FHIR/Resources\",\"resource\":{\"resourceType\":\"ServiceRequest\",\"id\":\"example_request_HR\"," +
                                "\"status\":\"active\",\"intent\":\"original-order\",\"code\":{\"coding\":[{\"system\":\"http://loinc.org\",\"code\":\"59408-5\"," +
                                "\"display\":\"Oxygen saturation in Arterial blood by Pulse oximetry\"}],\"text\":\"Oxygen saturation in Arterial blood by Pulse oximetry\"}," +
                                "\"subject\":{\"reference\":\"Patient/patient_example\"}}}],\"resourceType\":\"Bundle\",\"type\":\"collection\"}";
                        //"{\\\"entry\\\":[{\\\"fullUrl\\\":\\\"http://iprdgroup.com/FHIR/Resources\\\",\\\"resource\\\":{\\\"resourceType\\\":\\\"ServiceRequest\\\",\\\"id\\\":\\\"example_request_HR\\\",\\\"status\\\":\\\"active\\\",\\\"intent\\\":\\\"original-order\\\",\\\"code\\\":{\\\"coding\\\":[{\\\"system\\\":\\\"http://loinc.org\\\",\\\"code\\\":\\\"8867-4\\\",\\\"display\\\":\\\"Heart rate\\\"}],\\\"text\\\":\\\"Heart rate\\\"},\\\"subject\\\":{\\\"reference\\\":\\\"Patient\\/patient_example\\\"}}},{\\\"fullUrl\\\":\\\"http://iprdgroup.com/FHIR/Resources\\\",\\\"resource\\\":{\\\"resourceType\\\":\\\"ServiceRequest\\\",\\\"id\\\":\\\"example_request_HR\\\",\\\"status\\\":\\\"active\\\",\\\"intent\\\":\\\"original-order\\\",\\\"code\\\":{\\\"coding\\\":[{\\\"system\\\":\\\"http://loinc.org\\\",\\\"code\\\":\\\"59408-5\\\",\\\"display\\\":\\\"Oxygen saturation in Arterial blood by Pulse oximetry\\\"}],\\\"text\\\":\\\"Oxygen saturation in Arterial blood by Pulse oximetry\\\"},\\\"subject\\\":{\\\"reference\\\":\\\"Patient/patient_example\\\"}}}],\"resourceType\\\":\\\"Bundle\\\",\\\"type\\\":\\\"collection\\\"}";

                isSpo2ObjAdded = isObjectAdded(SPO2_CODE);
                if(!isSpo2ObjAdded) {
                    Entry e = getEntry(SPO2_CODE, "Oxygen saturation","CarePlan 1", "CarePlan/id1560954676957", "http://loinc.org");
                    requestDataModel.entry.add(e);
                }

                isHeartRateObjAdded = isObjectAdded(HEARTRATE_CODE);
                if(!isHeartRateObjAdded) {
                    Entry e1 = getEntry(HEARTRATE_CODE, "Heart rate","CarePlan 1", "CarePlan/id1560954676957", "http://loinc.org");
                    requestDataModel.entry.add(e1);
                }
                Intent sendIntent = new Intent();
                sendIntent.putExtra("serviceRequest",svcReqJSON);
                sendIntent.setType("text/json");
                Intent chooser = new Intent(EmptyPlaceholders.this, OximeterOrThermometer.class);
                if (sendIntent.resolveActivity(getPackageManager()) != null) {
                    chooser.putExtra("serviceRequest",svcReqJSON);
                    chooser.setType("text/json");
                    Log.d("---1--","-----");
                    startActivityForResult(chooser, REQUEST_CODE1);
                }
            }
        });

        Button pTemperature = (Button) findViewById(R.id.temperatureBtn);
        pTemperature.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param v
             */
            @Override
            public void onClick(View v) {
                //if (m_state == PlaceholderState.HCW_DEVICES_MODULE) return;
                String svcReqJSON = "{\"entry\":[{\"fullUrl\":\"http://iprdgroup.com/FHIR/Resources\"," +
                        "\"resource\":{\"resourceType\":\"ServiceRequest\",\"id\":\"example_request_HR\"," +
                        "\"status\":\"active\",\"intent\":\"original-order\",\"code\":{\"coding\":" +
                        "[{\"system\":\"http://loinc.org\",\"code\":\"8310-5\",\"display\":" +
                        "\"Body temperature special circumstances\"}],\"text\":\"Body Temperature\"}," +
                        "\"subject\":{\"reference\":\"Patient/patient_example\"}}}],\"resourceType\":" +
                        "\"Bundle\",\"type\":\"collection\"}";

                isTrmpObjAdded = isObjectAdded(THINKMD_TEMPERATURE_CODE);
                if(!isTrmpObjAdded) {
                    Entry e = getEntry(THINKMD_TEMPERATURE_CODE, "Temperature","CarePlan 1", "CarePlan/id1560954676957", "http://loinc.org");
                    requestDataModel.entry.add(e);
                    Log.d("####isTrmpObjAdded [temp]#######", isTrmpObjAdded.toString());

                }
                Intent sendIntent = new Intent();
                sendIntent.putExtra("serviceRequest",svcReqJSON);
                sendIntent.setType("text/json");
                Intent chooser = new Intent(EmptyPlaceholders.this, OximeterOrThermometer.class);
                if (sendIntent.resolveActivity(getPackageManager()) != null) {
                    chooser.putExtra("serviceRequest",svcReqJSON);
                    chooser.setType("text/json");
                    Log.d("---2--","-----");
                    startActivityForResult(chooser, REQUEST_CODE1);
                }
            }
        });

        Button pHealthCube = (Button) findViewById(R.id.healthCubeBtn);
        pHealthCube.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param v
             */
            @Override
            public void onClick(View v) {
                try {
                    Intent launcherIntent = getPackageManager().getLaunchIntentForPackage(ezdxPackageName);
                    if (launcherIntent != null) {
                        launcherIntent.setPackage(getPackageName());
                        launcherIntent.setAction(EmptyPlaceholders.class.getCanonicalName());
                        if (patientDataInHl7String != null) {
                            launcherIntent.putExtra("PATIENT_DETAILS", patientDataInHl7String); // patient data in h7 format json string
                            launcherIntent.putExtra("TEST_DETAILS", hightlightTests); // list of testnames. pleae check R.string.testNames for all the test array Ezdx supports
                            if (sampleUserId != null && !sampleUserId.isEmpty() && secretKey != null && !secretKey.isEmpty()) { // login id and encrypted secret needs to sent every time
                                launcherIntent.putExtra("PARTNER_LOGIN_ID", sampleUserId); // user-id
                                launcherIntent.putExtra("PARTNER_LOGIN_SECRET", encrypt(secretKey, EmptyPlaceholders.this));
                            }
                            startActivity(launcherIntent);
                        } else {
                            Toast.makeText(EmptyPlaceholders.this, "Please enter correct data", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    } else {
                        // this will launch playstore if app is not installed
                        launcherIntent = new Intent(Intent.ACTION_VIEW);
                        launcherIntent.setData(Uri.parse("market://details?id=" + ezdxPackageName));
                        startActivity(launcherIntent);
                    }

                } catch (Exception e) {
                    Toast.makeText(EmptyPlaceholders.this, "Error launching ezdx " + e, Toast.LENGTH_SHORT).show();
                }
            }
        });

        m_mgdMalaria = (Button) findViewById(R.id.GDM);
        m_mgdMalaria.setOnClickListener(new View.OnClickListener() {
            private Object ResourcePatient;

            /**
             *
             * @param view
             */
            @Override
            public void onClick(View view) {

                SharedPreferences prefsMgd = PreferenceManager.getDefaultSharedPreferences(EmptyPlaceholders.this);
                String selectedPreferenceMgd = prefsMgd.getString("listpref", "MedsInc View");
               // hardcodeValues();
                if(selectedPreferenceMgd.equalsIgnoreCase("MedsInc View")){
                    MedsIncUI();
                }else {
                    InitialQuestionaireUI();
                }
            }

            /**
             *
             */
            private void hardcodeValues() {
                LoginProperties.getLoginInstance(getApplicationContext()).setpHeartRate(65);
                LoginProperties.getLoginInstance(getApplicationContext()).setHrUnit("bpm");
                LoginProperties.getLoginInstance(getApplicationContext()).setpSPO2(98.7f);
                LoginProperties.getLoginInstance(getApplicationContext()).setpTemperature(37.2f);
                LoginProperties.getLoginInstance(getApplicationContext()).setTempUnit("C");
            }

            /**
             *
             */
            private void MedsIncUI(){
                String s="{\n" +
                        "   \"scheme\": \"iprd\",\n" +
                        "   \"host\": \"com.dhi.iprd\",\n" +
                        "   \"package\": \"com.dhi.mgd\"\n" +
                        "}";
                String urldata="";
                try {
                    Boolean isEntry4Patient = true;
                    Boolean isEntry4ResourceEncounter = true;
                    Boolean isEentry4ResourcePractitioner = true;

                    for( int i=0; i<requestDataModel.entry.size(); i++){
                         Object entryObj = requestDataModel.entry.get(i).resource;
                         if(entryObj.getClass().getSimpleName().equalsIgnoreCase("ResourcePatient") && isEntry4Patient == true){
                             isEntry4Patient = false;
                         }
                        if(entryObj.getClass().getSimpleName().equalsIgnoreCase("ResourceEncounter") && isEntry4ResourceEncounter == true){
                            isEntry4ResourceEncounter = false;
                        }
                        if(entryObj.getClass().getSimpleName().equalsIgnoreCase("ResourcePractitioner") && isEentry4ResourcePractitioner == true){
                            isEentry4ResourcePractitioner = false;
                        }
                        if(!isEntry4Patient && !isEntry4ResourceEncounter && !isEentry4ResourcePractitioner)
                            break;

                    }
                    Log.d("~~~~~~",isEntry4Patient+","+isEntry4ResourceEncounter+","+isEentry4ResourcePractitioner);

                    if(isEntry4Patient == true){
                        addResourcePatient();
                    }
                    if(isEntry4ResourceEncounter == true){
                        addResourceEncounter();
                    }
                    if(isEentry4ResourcePractitioner == true) {
                        addResourcePractitioner();
                    }
                    ObjectMapper objectMapper = new ObjectMapper();
                    requestDataModelString = objectMapper.writeValueAsString(requestDataModel);
                    String requestDataModelEntryString = objectMapper.writeValueAsString(requestDataModel.entry);
                    Log.d("requestDataModelEntryString", requestDataModelEntryString);
                    String inp =requestDataModelString==null?"":requestDataModelString;
                    Log.d("requestDataModelString : >> ",requestDataModelString);
                    urldata = EncodeDecode.EncodeJSONforURL(inp);
                   
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                String prefix= EncodeDecode.URLEncode(s);

                //the URL below is a staging URL that was initially suggested to us by thinkMD for the Medsinc app
                //String finalURL = "https://medsinc-v3-app-staging.herokuapp.com/iprd/#/?androidIntent=" + prefix+ "&fhir="+urldata;
                //this is the medsinc production server. this was added for testing
                String finalURL = "https://app.medsinc.org/iprd/#/?androidIntent=" + prefix+ "&fhir="+urldata;
                Log.d("Final URL -->",finalURL);
                Uri uri = Uri.parse("googlechrome://navigate?url="+finalURL);
                Intent i = new Intent(Intent.ACTION_VIEW, uri);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }

            private void InitialQuestionaireUI() {
                Intent i = new Intent(EmptyPlaceholders.this, Questionnaire.class);
                if( Utils.GetUtils().getQuestionsList().size()==0) {
                    ArrayList<Question> q = new ArrayList<Question>();
                    if (Utils.FindQuestions(q)){
                        SetGrammerInQuestion(i, q);
                    } else {
                        Utils.LoadQuestions(getApplicationContext());
                        if (Utils.FindQuestions(q)){
                            SetGrammerInQuestion(i, q);
                        }
                        else {
                            Toast.makeText(localCtx, "Failure loading diagnostics",Toast.LENGTH_LONG).show();
                        }
                      //  finish();
                    }
                }
                else{
                    finish();
                    startActivity(i);
                }
            }
        });

        ImageButton bckButton = (ImageButton) findViewById(R.id.emptyholderbckBtn);
        bckButton.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param view
             */
            @Override
            public void onClick(View view) {
                Intent i = null;
                Class<? extends AppCompatActivity> destClass = null;
                PlaceholderState p = PlaceholderState.NULL;
                switch (m_state) {
                    case HCW_DEVICES_NONE:
                        i = new Intent(EmptyPlaceholders.this, MainActivity.class);
                        p = PlaceholderState.NULL;
                        break;
                    case HCW_DEVICES_MODULE:
                        i = new Intent(EmptyPlaceholders.this, EmptyPlaceholders.class);
                        p = PlaceholderState.HCW_DEVICES_NONE;
                        break;
                    case PATIENT_DEVICES_MEASUREMENT:
                        i = new Intent(EmptyPlaceholders.this, PatientEnroll.class);
                        p = PlaceholderState.NULL;
                        break;
                    case PRIORS:
                        i = new Intent(EmptyPlaceholders.this, EmptyPlaceholders.class);
                        p = PlaceholderState.PATIENT_DEVICES_MEASUREMENT;
                        break;
                    case MGD_PRIORS:
                        i = new Intent(EmptyPlaceholders.this, EmptyPlaceholders.class);
                        p = PlaceholderState.PRIORS;
                        break;
                    case TREATMENTS:
                        i = new Intent(EmptyPlaceholders.this, EmptyPlaceholders.class);
                        p = PlaceholderState.MGD_PRIORS;
                        break;
                    case ASSESMENTRESULT:
                        i = new Intent(EmptyPlaceholders.this, EmptyPlaceholders.class);
                        p = PlaceholderState.MGD_PRIORS;
                    default:
                }
                i.putExtras(getIntent());
                i.putExtra(getResources().getString(R.string.placehoder_enum), p);
                finish();
                startActivity(i);
            }
        });

         Bundle extras = getIntent().getExtras();
        if (extras != null) {
            m_state = (PlaceholderState) extras.getSerializable(getResources().getString(R.string.placehoder_enum));
            ParseMedsInc(extras);
        }

        mListView = (ListView) findViewById(R.id.deviceAvailableList);
        modelArrayList = getModel(false);
        customAdapter = new CustomAdapter(this,modelArrayList);
        mListView.setAdapter(customAdapter);

        switch (m_state) {
            case HCW_DEVICES_NONE:
                Button button = (Button) findViewById(R.id.noneBtn);
                button.setVisibility(View.VISIBLE);
                button.setClickable(false);
                m_msg = getResources().getString(R.string.devicenonelblName);
                setImageButtonVisibility(R.id.patientLgnPlhd, View.INVISIBLE);
//                button = (Button) findViewById(R.id.healthCubeBtn);
//                button.setVisibility(View.VISIBLE);
                break;

            case HCW_DEVICES_MODULE:

                mListView = (ListView) findViewById(R.id.deviceAvailableList);//???
//                // Create an ArrayAdapter from List
//                final ArrayAdapter<String> aAdapter = new ArrayAdapter<String>
//                        (this, android.R.layout.simple_list_item_1, availableDevices){
//                    @Override
//                    public View getView(int position, View convertView, ViewGroup parent){
//                        // Get the current item from ListView
//                        View view = super.getView(position,convertView,parent);
//                        if(position == 0) {
//                            Setbackground(view,  checkIntentAvailability(hearrateapp));
//                        }else if(position == 1){
//                            Setbackground(view, checkIntentAvailability(respirationrateapp)||true);
//                        }else if(position == 2){
//                            Setbackground(view, checkIntentAvailability(respirationrateapp)||true);
//                        }else if(position == 3){
//                            Setbackground(view,  checkIntentAvailability(ezdxPackageName));
//                        }
//                        return view;
//                    }
//                };
//                mListView.setAdapter(aAdapter); //??
                mListView.setVisibility(View.VISIBLE);

                mAl = new ArrayList<Model>();
                mAl.add(getModelTrueOrFalse(checkIntentAvailability(hearrateapp), 0));
                mAl.add(getModelTrueOrFalse(checkIntentAvailability(respirationrateapp), 1));
                mAl.add(getModelTrueOrFalse(checkIntentAvailability(respirationrateapp), 2));
                mAl.add(getModelTrueOrFalse(checkIntentAvailability(ezdxPackageName), 3));

                customAdapter = new CustomAdapter(EmptyPlaceholders.this,mAl);
                mListView.setAdapter(customAdapter);
                linearLayout4deviceList.setVisibility(View.VISIBLE);
                m_msg = getResources().getString(R.string.devicemodulelblName);
                setImageButtonVisibility(R.id.patientLgnPlhd, View.INVISIBLE);
                break;

            case PATIENT_DEVICES_MEASUREMENT:
                button = (Button) findViewById(R.id.heartRateBtn);
                setButton(button, hearrateapp);

                button = (Button) findViewById(R.id.respirationBtn);
                setButton(button, respirationrateapp);

                button = (Button) findViewById(R.id.temperatureBtn);
                setButton(button, respirationrateapp);

                button = (Button) findViewById(R.id.healthCubeBtn);
                setButton(button, ezdxPackageName);

                m_msg = "Available Internal Objective Measurement";
                setImageButtonVisibility(R.id.patientLgnPlhd, View.INVISIBLE);
                break;

            case PRIORS:
                TextView seasprior = (TextView) findViewById(R.id.txtseasonalprior);
                seasprior.setVisibility(View.VISIBLE);
                Spinner spinner = (Spinner) findViewById(R.id.seasonalPrior);
                spinner.setBackground(getResources().getDrawable(R.drawable.spinner_arrow_bg, null));
                ArrayList<String> seasonalArr = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.seasonalPrior)));
                if (seasonalArr.size() == 0)
                    seasonalArr.add("No seasonal entry");
                addItemsToSpinner(R.id.seasonalPrior, seasonalArr);

                TextView nearprior = (TextView) findViewById(R.id.txtnearbyprior);
                nearprior.setVisibility(View.VISIBLE);
                spinner = (Spinner) findViewById(R.id.nearbyPrior);
                spinner.setBackground(getResources().getDrawable(R.drawable.spinner_arrow_bg, null));
                ArrayList<String> nearByPriorArr = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.nearbyPrior)));
                if (nearByPriorArr.size() == 0)
                    nearByPriorArr.add("No nearby entries");
                addItemsToSpinner(R.id.nearbyPrior, nearByPriorArr);

                TextView pprior = (TextView) findViewById(R.id.txtpatientprior);
                pprior.setVisibility(View.VISIBLE);
                spinner = (Spinner) findViewById(R.id.patientPrior);
                spinner.setBackground(getResources().getDrawable(R.drawable.spinner_arrow_bg, null));
                ArrayList<String> priorsArr = new ArrayList<>();
                priorsArr.add("None");
                if (SessionProperties.getSessionInstance(mCtx).getPatientName() != null && SessionProperties.getSessionInstance(mCtx).getPatientName().length() > 0)
                    priorsArr.add(SessionProperties.getSessionInstance(mCtx).getPatientName());
                else {
                    Log.d("SpinnerError","Invalid drop down option");
                    priorsArr.add("Error in entry");
                }

                addItemsToSpinner(R.id.patientPrior, priorsArr);
                m_msg = getResources().getString(R.string.priorlblName);
                setImageButtonVisibility(R.id.patientLgnPlhd, View.VISIBLE);
                break;

            case MGD_PRIORS:
                button = (Button) findViewById(R.id.GDM);
                button.setEnabled(true);
                button.setVisibility(View.VISIBLE);
                preferrenceSettingBtnMgd.setVisibility(View.VISIBLE);
                m_msg = getResources().getString(R.string.MGDlblName);
                setImageButtonVisibility(R.id.patientLgnPlhd, View.VISIBLE);
                break;

            case TREATMENTS:
                TextView txtloc = (TextView) findViewById(R.id.txtlocprior);
                txtloc.setVisibility(View.VISIBLE);
                spinner = (Spinner) findViewById(R.id.treatmentLocation);
                spinner.setBackground(getResources().getDrawable(R.drawable.spinner_arrow_bg, null));
                addItemsToSpinner(R.id.treatmentLocation, R.array.treatmentLocation);
                m_msg = getResources().getString(R.string.trelblName);
                setImageButtonVisibility(R.id.patientLgnPlhd, View.VISIBLE);
                break;
            case ASSESMENTRESULT:
                m_msg = "Calling: Assesment Module";
                //m_nxtButton.setEnabled(false);
                m_nxtButton.setBackground(getResources().getDrawable(R.drawable.grey_rounded_background,null));
                m_nxtButton.setEnabled(false);
                m_AssesmentResult.setVisibility(View.VISIBLE);
                String tobesent = JSONGenerator.generateRequestJSON(Utils.GetUtils().getQuestionsList());
                m_AssesmentResult.setText("Connecting to server for fetching Assessment.... ");
                String Disease="malaria";
                AsyncTaskRunner runner = new AsyncTaskRunner();
                //if (tobesent != null && tobesent.length() > 0)
                runner.execute(Disease,tobesent,Disease);
                setImageButtonVisibility(R.id.patientLgnPlhd, View.VISIBLE);
                break;
            case HEALTH_STATS:
                break;
            case NULL:
                break;
            default:
        }
        m_plcTxtView.setText(m_msg);

        m_newPatientButton.setOnClickListener(new View.OnClickListener() {
            @Override
            /**
             *
             */
            public void onClick(View view) {
                Log.d("requestDataModel >>>>>"," making null ");
                requestDataModel = null;
                requestDataModel = new RequestDataModel();
                Intent i = new Intent(
                        EmptyPlaceholders.this,
                        PatientEnroll.class);
                finish();
                startActivity(i);
            }
        });

        if (m_state == PlaceholderState.HEALTH_STATS) m_nxtButton.setText("DONE");
    }


    private void addResourceEncounter() {
        ResourceEncounter resourceEncounter = new ResourceEncounter();
        resourceEncounter.id = "id1560452012016";
        resourceEncounter.status = "arrived";
        resourceEncounter.text.put("div","<div xmlns='http://www.w3.org/1999/xhtml'>Encounter with patient @example</div>");
        resourceEncounter.text.put("status","additional");

        Identifier idntfr = new Identifier();
        idntfr.value = "id1560451639623";
        idntfr.system = "reference";
        resourceEncounter.identifier.add(idntfr);
        resourceEncounter.status = "arrived";

        resourceEncounter.Class.put("system","http://hl7.org/fhir/v3/ActCode");
        resourceEncounter.Class.put("code","HH");
        resourceEncounter.Class.put("display","home health");
        resourceEncounter.subject.put("reference","id1560451639623");
        resourceEncounter.subject.put("display","child");

        ResourceLocation resourceLocation = new ResourceLocation();
        resourceLocation.resourceType = "Location";
        resourceLocation.id = "id456890234";
        resourceLocation.status = "status";
        resourceLocation.name = "Patient's Home";
        resourceLocation.description = "Patient's Home";
        resourceLocation.mode = "kind";
        PhysicalType physicalType = new PhysicalType();
        Coding cdng = new Coding("http://terminology.hl7.org/CodeSystem/location-physical-type","ho","House");

        physicalType.coding.add(cdng);

        resourceLocation.physicalType = physicalType;
        resourceEncounter.contained.add(resourceLocation);

        Individual indvsl = new Individual();
        indvsl.individual = "id1560454981713";
        resourceEncounter.participant.add(indvsl) ;
        Location ll = new Location();
        ll.location = "id456890234";
        resourceEncounter.location.add(ll) ;

        Entry ee = new Entry();
        ee.resource = resourceEncounter;
        requestDataModel.entry.add(ee);
    }

    private void addResourcePractitioner(){

        ResourcePractitioner resourcePractitioner = new ResourcePractitioner();
        resourcePractitioner.id = LoginProperties.getLoginInstance(getApplicationContext()).getHGUID();//"id1560454981713";
        resourcePractitioner.active = true;

        Identifier idntfr = new Identifier();
        idntfr.system = "example";
        resourcePractitioner.identifier.add(idntfr);


        Name nme = new Name();
        nme.family = "Careful";
        HashMap<String, ArrayList<String>> map = new HashMap<>();

        ArrayList<String> al = new ArrayList<>();
        al.add("Mr");
        map.put("prefix",al);

        ArrayList<String> alst = new ArrayList<>();
        alst.add(LoginProperties.getLoginInstance(getApplicationContext()).getHName());
        map.put("given",alst);

        nme.mapA = map;

        resourcePractitioner.name.add(nme);


        Entry ee = new Entry();
        ee.resource = resourcePractitioner;
        requestDataModel.entry.add(ee);
    }

    private void addResourcePatient() {
        ResourcePatient resourcePatient = new ResourcePatient();
        resourcePatient.birthDate = LoginProperties.getLoginInstance(getApplicationContext()).getPDOB();
        resourcePatient.gender = "Male";//LoginProperties.getLoginInstance(getApplicationContext()).getPgender();
        resourcePatient.id = LoginProperties.getLoginInstance(getApplicationContext()).getPGUID();//"example";
        resourcePatient.text.put("status","generated");
        resourcePatient.text.put("div","generated");
        ArrayList<Identifier> identifier = new ArrayList<>();
        Identifier identifier1 = new Identifier();
        identifier1.system = "urn:oid:2.16.840.1.113883.19.5";
        identifier1.value = "12345";
        identifier1.use = "usual";
        //ArrayList<Coding> coding = new ArrayList<>();
        Coding cding = new Coding("http://terminology.hl7.org/CodeSystem/v2-0203","MR","");
//        cding.code = "MR";
//        cding.system = "http://terminology.hl7.org/CodeSystem/v2-0203";
       // coding.add(cding);

        identifier1.type.coding.add(cding);
        resourcePatient.identifier.add(identifier1);

        resourcePatient.active =true;

        Name nme = new Name();
        nme.family = "1";
        nme.given.add(LoginProperties.getLoginInstance(getApplicationContext()).getpName());
        resourcePatient.name.add(nme);


        StringBuffer dobStr = new StringBuffer();
        String pDOBStr ="";
        long MILLIS_IN_DAY = 24*60*60*1000;
        pDOBStr = LoginProperties.getLoginInstance(getApplicationContext()).getPDOB();
        Log.d("DOB from Props",pDOBStr.toString());
        //if(pDOBStr1.length() > 0)
          //  pDOBStr = Integer.toString(Integer.parseInt(pDOBStr1)-31);
        if (pDOBStr != null && pDOBStr.length() > 0)
        {
            Date date = new Date(Long.parseLong(pDOBStr)*MILLIS_IN_DAY);
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            dobStr.append(format.format(date));
        }
        else
            dobStr.append("2017-01-30");

        Log.d("DOB from props in good format",dobStr.toString());
       resourcePatient.birthDate = dobStr.toString();

        Entry ee = new Entry();
        ee.resource = resourcePatient;
        requestDataModel.entry.add(ee);
    }

    private Boolean isObjectAdded(String code){//THINKMD_SPO2_CODE
        ResourceObservation resourceObservationSpo2Res = new ResourceObservation();
        Boolean isSpo2ObjAdded = false;
        for(Entry rdEntry: requestDataModel.entry){
            if("ResourceObservation".equalsIgnoreCase(rdEntry.resource.getClass().getSimpleName())){
                resourceObservationSpo2Res = (ResourceObservation) rdEntry.resource;
                if(resourceObservationSpo2Res.code.coding.size() > 0 && resourceObservationSpo2Res.code.coding.get(0).code.equalsIgnoreCase(code)){
                    isSpo2ObjAdded = true;
                    break;
                }
            }
        }
        return isSpo2ObjAdded;
    }

    private Entry getEntry(String thinkmd_spo2_code, String s, String display, String reference, String system) {
        ResourceObservation resourceObservation = new ResourceObservation();
        resourceObservation.id = "id1560954449058";
        resourceObservation.status = "preliminary";

        Log.d("Dispalay :", display);
        Log.d("Reference :", reference);

        BasedOn bOn = new BasedOn();
        bOn.display = display;
        bOn.reference = reference;
        resourceObservation.basedOn.add(bOn);

        ArrayList<Coding> codeList = new ArrayList<>();

        Coding coding = new Coding(system,thinkmd_spo2_code,s);

        codeList.add(coding);
        Code code = new Code();
        code.coding = codeList;

        resourceObservation.code = code;
        //String pattern = "YYYY-MM-dd";
        String pattern ="YYYY-MM-dd'T'HH:mm:ss.SSSXXX";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

        resourceObservation.effectiveDateTime = simpleDateFormat.format(new Date());//new Date().toString();
        System.out.println("resourceObservation.effectiveDateTime : "+simpleDateFormat.format(new Date()));
        //resourceObservation.subject = new Subject();

        Entry e = new Entry();
        e.resource = resourceObservation;
        return e;
    }

    private void setButton(Button button, String ezdxPackageName) {
        if (checkIntentAvailability(ezdxPackageName)) {
            button.setBackgroundResource(R.drawable.green_rounded_background);
        } else {
            button.setBackgroundResource(R.drawable.grey_rounded_background);
            button.setClickable(false);
        }
        button.setVisibility(View.VISIBLE);
    }


    private void SetGrammerInQuestion(Intent i, ArrayList<Question> q) {
        for (int k = 0; k < q.size(); k++) {
            String patientType = "child";
            String audience = "external";
            StringBuffer grRsp = new StringBuffer();
            String txt = q.get(k).getText();
            boolean b = interpretGrammar(txt, patientType, audience, grRsp);
            if (b){
                q.get(k).setText(grRsp.toString());
            }
        }
        Utils.GetUtils().setQuestionsList(q);
        finish();
        startActivity(i);
    }

    private boolean ParseMedsInc(Bundle extras) {
        String value1 = extras.getString("bundle");
        boolean ret = false;
        if (value1 != null) {
            Log.d("ThinkMd Result","\n bundle -> " + value1.toString());
            String decodeddata = "";
            try {
                decodeddata = EncodeDecode.DecodeDatafromURL(value1);
                Log.d("Retdata",decodeddata);
                Log.d("Size of thinkMD result string",Integer.toString(decodeddata.length()));
            /*   int size  = 512;
                List<String> result = new ArrayList<String>((decodeddata.length() + size - 1) / size);
                for (int i = 0; i < decodeddata.length(); i += size) {
                    result.add(decodeddata.substring(i, Math.min(decodeddata.length(), i + size)));
                }
                Log.d("Size of result list - ",Integer.toString(result.size()));
                for (int i = 0;i<result.size();i++)
                {
                    System.out.println(result.get(i));
                } */

            } catch (IllegalArgumentException ex) {
                Log.e("thinkMD result error",ex.toString());
                //return false;
            }

            //Toast.makeText(this, "Bundle Result"+ decodeddata, Toast.LENGTH_LONG).show();
            String pGUID = LoginProperties.getLoginInstance(getApplicationContext()).getPGUID();
            if(pGUID != null && pGUID.length()>0) {
                m_state = PlaceholderState.TREATMENTS;
                m_nxtButton.callOnClick();
            }else{
                String hGUID = LoginProperties.getLoginInstance(getApplicationContext()).getHGUID();
                if(hGUID != null && hGUID.length()>0) {
                    Intent i = new Intent(EmptyPlaceholders.this,PatientEnroll.class);
                    finish();
                    startActivity(i);
                }else{
                    Intent i = new Intent(EmptyPlaceholders.this, MainActivity.class);
                    finish();
                    startActivity(i);
                }
            }
            ret = true;
        }
        return ret;
    }

    private void Setbackground(View view, boolean b) {
        if (b) {
            view.setBackgroundColor(Color.parseColor("#46b58f"));
        } else {
            view.setBackgroundColor(Color.parseColor("#ececec"));
        }
    }

    private ArrayList<String> getArrList(String t) {
        ArrayList<String> m_msgArr = new ArrayList<String>();
        m_msgArr.add(t);
        return m_msgArr;
    }

    private void SetIntent(Intent i, ArrayList<String> m_msgArr) {
        i.putExtra(getResources().getString(R.string.placehoder_enum), PlaceholderState.HCW_DEVICES_NONE);
        i.putStringArrayListExtra(getResources().getString(R.string.placehoder_array), m_msgArr);
    }

    private void setSpinnerVisibility(int Prior_id, int visibility) {
        Spinner spinner = (Spinner) findViewById(Prior_id);
        spinner.setVisibility(visibility);
    }

    private void setTextVisibility(int Prior_id, int visibility) {
        TextView txt = (TextView) findViewById(Prior_id);
        txt.setVisibility(visibility);
    }
    private void setImageButtonVisibility(int btnid, int visibility) {
        ImageButton b = (ImageButton) findViewById(btnid);
        b.setVisibility(visibility);
    }
    private void setButtonVisibility(int btnid, int visibility) {
        Button b = (Button) findViewById(btnid);
        b.setVisibility(visibility);
    }

    /**
     * for interprete the grammar.
     * @param inputString
     * @param patientType
     * @param audType
     * @param outStrRsp
     * @return
     */
    public boolean interpretGrammar(String inputString, String patientType,String audType, StringBuffer outStrRsp)
    {
        //we expect inputString in the following format: whaat is {noun}{plural} name?
        //ex: if above is for  patientType newborn, and audienceType external - it should return: what is the baby's name?

        //first read inputString. look for "{". if seen, what follows is a grammarVar till we encounter another "}".
        String outStr = "";
        boolean isGrammarVar = false;
        String grammarVar = "";
        for (int i = 0; i < inputString.length(); i++) {
            char c = inputString.charAt(i);
            if (c == OPENING_BRACE)
            {
                //what starts after this is a grammarVar
                isGrammarVar = true;
            }
            else if (c == CLOSING_BRACE)
            {
                //if we see this, most likely it means we were reading a grammar variable before this.
                //now stop interpreting it as a grammar var. the closinbg brace signifies that.
                if (isGrammarVar) {
                    isGrammarVar = false;
                    StringBuffer grammarRsp = new StringBuffer();
                    //also use whatever is in the grammarVar string and get its interpretation
                    if (!processJSONFile(getApplicationContext(),patientType,audType,grammarVar,grammarRsp))
                        return false;
                    else {
                        outStr = outStr + grammarRsp.toString();
                        //also make sure that grammarVar is reset back to "".
                        grammarVar = "";
                    }
                }
            }
            else
            {
                //if isGrammarVar is false, then copy char as is to the outStr.
                //if set to true, then what is coming up is a grammarVar.
                if (!isGrammarVar)
                    outStr = outStr + c;
                else
                {
                    //add this to grammarVar
                    grammarVar = grammarVar + c;
                }
            }
        }
        outStrRsp.append(outStr);
        return true;
    }


    private boolean processJSONFile(Context ctx, String patientType, String audType, String grammarVar, StringBuffer grRsp) {
        try {
            String jsonTxt = JSONGenerator.loadJSONFromAsset(ctx, R.string.grammar_json);;
            JSONObject grammarJS = new JSONObject(jsonTxt);
            String grammar = grammarJS.getString("patientType");
            JSONObject patientTypeGrammar = new JSONObject(grammar);
            Iterator<String> keys = patientTypeGrammar.keys();

            while(keys.hasNext()) {
                String key = keys.next();
                if (key.equals(patientType))
                {
                    if (patientTypeGrammar.get(key) instanceof JSONObject) {
                        // do something with jsonObject here

                        Log.d("Key",key);
                        JSONObject pTypeGrammar = (JSONObject) patientTypeGrammar.get(key);
                        JSONObject extGrammar = (JSONObject) pTypeGrammar.get("external");
                        JSONObject selfGrammar = (JSONObject) pTypeGrammar.get("self");
                        if (audType == "self")
                            grRsp.append(selfGrammar.getString(grammarVar));
                        else if (audType == "external") {
                            if (extGrammar.getString(grammarVar).equals(SUBJECT_NAME)) {
                                if (LoginProperties.getLoginInstance(ctx).getpName() != null && LoginProperties.getLoginInstance(ctx).getpName().length() > 0)
                                    grRsp.append(LoginProperties.getLoginInstance(ctx).getpName());
                                else
                                    grRsp.append("Patient");
                            }
                            else
                                grRsp.append(extGrammar.getString(grammarVar));
                        }
                        else
                            return false;
                        return true;
                    }
                }
            }
            return false;
        } catch (JSONException var3) {
            return false;
        }
    }

    private void addItemsToSpinner(int spinner_id, int itemsToDisplay) {
        Spinner spinner = (Spinner) findViewById(spinner_id);
        spinner.setVisibility(View.VISIBLE);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(EmptyPlaceholders.this, itemsToDisplay, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
    }

    private void addItemsToSpinner(int spinner_id, List<String> itemsToDisplay) {
        Spinner spinner = (Spinner) findViewById(spinner_id);
        spinner.setVisibility(View.VISIBLE);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(EmptyPlaceholders.this, android.R.layout.simple_spinner_item, itemsToDisplay);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
    }

    private void createIntent(Class<? extends AppCompatActivity> destClass, PlaceholderState state) {
        Intent i = new Intent(
                EmptyPlaceholders.this, destClass);
        i.putExtras(getIntent());
        i.putExtra(getResources().getString(R.string.placehoder_enum), state);
        finish();
        startActivity(i);
    }

    /**
     * intant call result back.
     * @param intent
     */
    @Override
    protected void onNewIntent(Intent intent) {

        super.onNewIntent(intent);
        Log.d("<<<<<","Empty place holder return");
        Bundle extras = intent.getExtras();
        if(!ParseMedsInc(extras)) {
            String dataezdx = intent.getStringExtra(ezdxPackageName + "RESULT");
            if (dataezdx != null) {
                try {
                    JSONObject jsonObEntry = new JSONObject(dataezdx);
                    JSONArray jsonArrayEntry = new JSONArray(jsonObEntry.getString("entry"));

                    HealthCubeObservationMap healthCubeObservationMap = new HealthCubeObservationMap();
                    for(int i=0; i<jsonArrayEntry.length(); i++){
                        String resourceObj = new JSONObject(jsonArrayEntry.getString(i)).getString("resource");
                       if( "Observation".equalsIgnoreCase(new JSONObject(resourceObj).getString("resourceType"))) {

                           if((!(new JSONObject(resourceObj).has("component")))){
                               String code = new JSONObject(new JSONArray(new JSONObject(new JSONObject(resourceObj).getString("code")).getString("coding")).getString(0)).getString("code");
                               String val = new JSONObject(new JSONObject(resourceObj).getString("valueQuantity")).getString("value");
                               String unit = new JSONObject(new JSONObject(resourceObj).getString("valueQuantity")).getString("unit");
                               Log.d(unit+"->>>1> ",code+""+val);
                               val = val;//(unit.equalsIgnoreCase("F") || unit.equalsIgnoreCase("°F") )? ifUnitIsF(val) : val;// only for demo purpose
                               unit = demoSetUnit(unit);// only for demo purpose
                               code = code!=null ? code.trim() : "";//demoSetCode(code.trim());// only for demo purpose

                               getHealthCubeEntryObject(healthCubeObservationMap, code, unit, val);
                           }else{
                               JSONArray componentArr =new JSONArray (new JSONObject(resourceObj).getString("component"));
                               for(int j=0; j < componentArr.length(); j++){
                                   JSONObject componentCoding = new JSONObject(new JSONArray(new JSONObject(new JSONObject(new JSONObject(componentArr.getString(j)).getString("resource")).getString("code")).getString("coding")).getString(0));
                                   Log.d("->>>2> ","");
                                   String code = componentCoding.getString("code");
                                   String code4Component = code!=null ? code.trim() : ""; //demoSetCode(componentCoding.getString("code"));
                                   JSONObject valQuaObj = new JSONObject(new JSONObject(new JSONObject(componentArr.getString(j)).getString("resource")).getString("valueQuantity"));
                                   String unit = valQuaObj.getString("unit");
                                   String val = valQuaObj.getString("value");
                                   getHealthCubeEntryObject(healthCubeObservationMap, code4Component, unit, val);
                               }
                           }
                       }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            Toast.makeText(this, "OnNewIntent called", Toast.LENGTH_SHORT).show();
        }
    }

    private void getHealthCubeEntryObject(HealthCubeObservationMap healthCubeObservationMap, String code4Component, String unit, String val) {
        if (healthCubeObservationMap.map.containsKey(code4Component) && (!isObjectAdded(code4Component))) {//
            Entry e = healthCubeObservationMap.map.get(code4Component);
            requestDataModel.entry.add(e);

        }
        setBiometriData(val, unit, code4Component,"http://unitsofmeasure.org");
    }

    /**
     * Map for checking the device result unit and set the unit (as required got IPRD App)
     * @param unitToBeCheck : incomming unit string
     * @return : String
     */
    public String demoSetUnit(String unitToBeCheck){
        HashMap<String, String> map = new HashMap<>();
        map.put("°F","F");
        //map.put("F","C");
        map.put("Kgs","kg");
        map.put("cms","cm");
        map.put("Fahrenheit","F");
        if(map.containsKey(unitToBeCheck)){
            return map.get(unitToBeCheck);
        }
        return unitToBeCheck;
    }
    /**
     *
     * method to encrypt secret-key
     * @param secretKey : secret key string
     * @param activity : Activity
     * @return : String
     * @throws Exception
     */
    public static String encrypt(String secretKey, Activity activity) throws Exception {

        byte[] srcBuff = secretKey.getBytes("UTF8");
        SecretKeySpec skeySpec = new
                SecretKeySpec((activity.getPackageName() + "ezdxandroid").substring(0, 16).getBytes(), "AES");
        IvParameterSpec ivSpec = new
                IvParameterSpec((activity.getPackageName() + "ezdxandroid").substring(0, 16).getBytes());
        Cipher ecipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
        ecipher.init(Cipher.ENCRYPT_MODE, skeySpec, ivSpec);
        byte[] dstBuff = ecipher.doFinal(srcBuff);
        String base64 = Base64.encodeToString(dstBuff, Base64.DEFAULT);
        return base64;
    }

    private ArrayList<Model> getModel(boolean isSelect){
        ArrayList<Model> list = new ArrayList<>();
        for(int i = 0; i < availableDevices.length; i++){

            Model model = new Model();
            model.setSelected(isSelect);
            model.setDeviceName(availableDevices[i]);
            list.add(model);
        }
        return list;
    }

    private Model getModelTrueOrFalse(boolean isSelect ,int arrayIndex){
        Model model = new Model();
        model.setSelected(isSelect);
        model.setDeviceName(availableDevices[arrayIndex]);
        return model ;
    }
}