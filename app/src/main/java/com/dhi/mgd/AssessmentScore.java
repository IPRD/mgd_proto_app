package com.dhi.mgd;

import java.util.ArrayList;

/**
 *
 * Date: Aug 30 2019
 * @author Apra Developers
 * @version 1.0
 */
public class AssessmentScore {
    public String Disease, Severity;
    public long ID;
    ArrayList<TreatmentResult> Treatment;
    protected AssessmentScore() {
        ID = -1;
        Disease = "";
        Severity = "";
        Treatment = new ArrayList<TreatmentResult>();
    }
    public ArrayList<TreatmentResult> getTreatment() {
        return Treatment;
    }

}

