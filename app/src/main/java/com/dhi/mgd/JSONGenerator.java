package com.dhi.mgd;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
/**
 *.
 * Date: Aug 30 2019
 * @author Apra Developers
 * @version 1.0
 */
public class JSONGenerator {
    static String Name = "JSONGenerator";
    static String grpIDKey = "GroupID";
    static String qIDKey = "ID";
    static String qTextKey = "Text";
    static String qTypeKey = "Type";
    static String qOptionsKey = "Options";
    static String qAssessmentKey = "assessments";
    static String qTreatmantKey="treatments";
    static Map<String, QuestionType> qTypeMap = new HashMap<String, QuestionType>() {{
        put("mcq", QuestionType.MCQ);
        put("bool", QuestionType.BOOL);
        put("int", QuestionType.NUMBER);
        put("float", QuestionType.REAL);
        put("string", QuestionType.STRING);
    }};

    /**
     * generates JSON Request from given list of question.
     * @param questionsArr : list of questions.
     * @return : String
     */
    public static String generateRequestJSON(ArrayList<Question> questionsArr) {
        if (questionsArr == null)
            return null;
            //throw new IllegalArgumentException("Question are unavailable");
        JSONArray requestJSON = new JSONArray();
        for (Question question : questionsArr) {
            String key = question.getID();
            String endKey = ((question.getType() == QuestionType.MCQ) || (question.getType() == QuestionType.BOOL)) ? "selected" : "value";
            for (String responseKey : question.getArrayResponse()) {
                JSONObject questionJSONObj = new JSONObject();
                String jsonKeyStr = key + "." + responseKey + "." + endKey;
                try {
                    switch (question.getType()) {
                        case REAL:
                            questionJSONObj.put(jsonKeyStr, question.getRealResponse());
                            break;
                        case NUMBER:
                            questionJSONObj.put(jsonKeyStr, question.getNumResponse());
                            break;
                        default:
                            boolean state = true;
                            questionJSONObj.put(jsonKeyStr, state);
                    }
                    requestJSON.put(questionJSONObj);
                } catch (JSONException ex) {
                    Log.e(Name, ex.getMessage());
                    ex.printStackTrace();
                    return null;
                }
            }
        }
        return requestJSON.toString();
    }

    /**
     *
     *  For getting assessments
     * @param jsonStr
     * @param groupID
     * @return
     */
    public static ArrayList<Question> getAssesmentss(String jsonStr, String groupID) {
        ArrayList<Question> parsedQuestions = new ArrayList<Question>();
        if (jsonStr == null)
            throw new IllegalArgumentException("Json string is unavailable");
        if (jsonStr.length() == 0)
            throw new IllegalArgumentException("Json string is unavailable");
        try {
            JSONArray jsonQuestions = new JSONArray(jsonStr.trim());
            for (int i = 0; i < jsonQuestions.length(); i++) {
                JSONObject questionJSONObj = jsonQuestions.getJSONObject(i);
                if (!checkObject(questionJSONObj, grpIDKey))
                    continue;
                String grpIDVal = questionJSONObj.getString(grpIDKey);
                if (!grpIDVal.contains(groupID))
                    continue;
                if (!checkObject(questionJSONObj, qOptionsKey))
                    continue;
                JSONObject optionsObj = questionJSONObj.getJSONObject(qOptionsKey);
                LinkedHashMap <String, String> optionsMap = new LinkedHashMap <String, String>();
                Iterator<String> keys = optionsObj.keys();
                while (keys.hasNext()) {
                    String key = keys.next();
                    optionsMap.put(key, optionsObj.getString(key));
                }
                if (optionsMap.size() == 0)
                    continue;
                parsedQuestions.add(new Question(groupID, "", "",QuestionType.STRING,  optionsMap));
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return parsedQuestions;
    }

    /**
     *
     * For getting the question
     * @param jsonStr
     * @param groupID
     * @param paitentType
     * @param audience
     * @return
     */
    public static ArrayList<Question> getQuestions(String jsonStr, String groupID, String paitentType, String audience) {
        ArrayList<Question> parsedQuestions = new ArrayList<Question>();
        if (jsonStr == null)
            throw new IllegalArgumentException("Json string is unavailable");
        if (jsonStr.length() == 0)
            throw new IllegalArgumentException("Json string is unavailable");
        try {
            JSONArray jsonQuestions = new JSONArray(jsonStr.trim());
            for (int i = 0; i < jsonQuestions.length(); i++) {
                JSONObject questionJSONObj = jsonQuestions.getJSONObject(i);
                if (!checkObject(questionJSONObj, grpIDKey))
                    continue;
                String grpIDVal = questionJSONObj.getString(grpIDKey);
                if (!grpIDVal.equals(groupID))
                    continue;

                String ID, Text;
                QuestionType Type;
                if (!checkObject(questionJSONObj, qIDKey))
                    continue;
                ID = questionJSONObj.getString(qIDKey);
                if (!checkObject(questionJSONObj, qTextKey))
                    continue;
                Text = questionJSONObj.getString(qTextKey);

                if (!checkObject(questionJSONObj, qTypeKey))
                    continue;
                String typeStr = questionJSONObj.getString(qTypeKey);
                if (!qTypeMap.containsKey(typeStr))
                    continue;
                Type = qTypeMap.get(typeStr);
                if (!checkObject(questionJSONObj, qOptionsKey))
                    continue;
                JSONObject optionsObj = questionJSONObj.getJSONObject(qOptionsKey);
                LinkedHashMap<String, String> optionsMap = new LinkedHashMap <String, String>();
                Iterator<String> keys = optionsObj.keys();
                while (keys.hasNext()) {
                    String key = keys.next();
                    optionsMap.put(key, optionsObj.getString(key));
                }
                if (optionsMap.size() == 0)
                    continue;
                parsedQuestions.add(new Question(groupID, ID, Text, Type, optionsMap));
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return parsedQuestions;
    }

    /**
     * parse assessment response from given given json string in parameter.
     * @param jsonStr
     * @return
     */
    public static ArrayList<AssessmentScore> parseAssessmentResponse(String jsonStr) {
        if ((jsonStr == null) || (jsonStr.length() == 0))
            throw new IllegalArgumentException("Json string is unavailable");
        ArrayList<AssessmentScore> response = new ArrayList<AssessmentScore>();
        try {
            JSONArray jsonResponses = new JSONArray(jsonStr.trim());
            for (int i = 0; i < jsonResponses.length(); i++) {
                JSONObject jsonObject = jsonResponses.getJSONObject(i);
                if (!checkObject(jsonObject, qAssessmentKey))
                    continue;
                JSONArray assessmentArr = jsonObject.getJSONArray(qAssessmentKey);
                for (int j = 0; j < assessmentArr.length(); j++) {
                    JSONObject assessmentObj = assessmentArr.getJSONObject(j);
                    Iterator<String> keys = assessmentObj.keys();
                    AssessmentScore aScore = new AssessmentScore();
                    while (keys.hasNext()) {
                        String key = keys.next();
                        if (key.equals("id")) {
                            aScore.ID = assessmentObj.getInt(key);
                            continue;
                        }
                        if (key.equals("name")) {
                            aScore.Disease = assessmentObj.getString(key);
                            continue;
                        }
                        if (key.equals("severity")) {
                            aScore.Severity = assessmentObj.getString(key);
                            continue;
                        }
                        if (key.equals(qTreatmantKey)) {
                            ArrayList<TreatmentResult> tarr= new ArrayList<TreatmentResult>();
                            JSONArray arr = assessmentObj.getJSONArray(qTreatmantKey);
                            for (int k = 0; k < arr.length(); k++) {
                                JSONObject arrObj = arr.getJSONObject(k);
                                Iterator<String> l = arrObj.keys();
                                TreatmentResult t = new TreatmentResult();
                                while (l.hasNext()) {
                                    String keyl = l.next();
                                    if (keyl.equals("id")) {
                                        t.ID = arrObj.getInt(keyl);
                                        continue;
                                    }
                                    if (keyl.equals("name")) {
                                        t.Name = arrObj.getString(keyl);
                                        continue;
                                    }
                                    if (keyl.equals("category")) {
                                        t.Catagory = arrObj.getString(keyl);
                                        continue;
                                    }
                                }
                                tarr.add(t);
                            }
                            aScore.Treatment = tarr;
                            continue;
                        }
                    }
                    response.add(aScore);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return response;
    }

    /**
     *
     * @param jsonObj
     * @param id
     * @return
     */
    private static boolean checkObject(JSONObject jsonObj, String id) {
        if (!jsonObj.has(id))
            return false;
        if (jsonObj.isNull(id))
            return false;
        return true;
    }

    /**
     * loads JSON from asset
     * @param context
     * @param id
     * @return
     */
    public static String loadJSONFromAsset(Context context, int id) {
        String json = null;
        if (context == null)
            throw new IllegalArgumentException("Context unavailable");
        Resources res = context.getResources();
        try {
            InputStream is = res.getAssets().open(res.getString(id));
            int size = is.available();
            if (size == 0)
                return null;
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
