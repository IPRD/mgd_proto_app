package com.dhi.mgd;

import android.gesture.Gesture;
import android.gesture.GestureOverlayView;
import android.gesture.GestureOverlayView.OnGesturePerformedListener;
import android.support.v7.app.AppCompatActivity;

/**
 *.
 * Date: Aug 30 2019
 * @author Apra Developers
 * @version 1.0
 */
public class CustomGestureListener extends AppCompatActivity implements OnGesturePerformedListener {

    /**
     *
     * @param gestureOverlayView
     * @param gesture
     */
    @Override
    public void onGesturePerformed(GestureOverlayView gestureOverlayView, Gesture gesture) {

    }
}