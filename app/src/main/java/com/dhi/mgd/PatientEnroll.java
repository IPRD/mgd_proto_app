package com.dhi.mgd;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.dhi.RequestResponse.RequestDataModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.dhi.mgd.EmptyPlaceholders.requestDataModel;

/**
 *.
 * Date: Aug 30 2019
 * @author Apra Developers
 * @version 1.0
 */
public class PatientEnroll extends AppCompatActivity {
    private Context mCtx;
    private EditText patientName;
    private EditText patientPhone;
    private Spinner patientCountry;
    private EditText patientDOB;
    Intent sendIntent;
    Button nxtButton;
    final String CONNECT = "ConnectToIdentity";
    final String ADD_PERSON = "AddPerson";

    final char CLOSING_BRACE = ')';
    final char PHONE_INDICATOR = '+';

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_patient_enroll);
        patientName = (EditText) findViewById(R.id.patientNameTxt);
        patientPhone = (EditText) findViewById(R.id.patientPhoneno);
        patientDOB = (EditText) findViewById(R.id.patientDob);
        patientCountry = (Spinner) findViewById(R.id.patientCountryCode);
        nxtButton = (Button) findViewById(R.id.patientEnrlNextBtn);
        final Calendar myCalendar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            /**
             *
             * @param view
             * @param year
             * @param monthOfYear
             * @param dayOfMonth
             */
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel(patientDOB,myCalendar);
            }

        };

        mCtx = this.getApplicationContext();

        patientDOB.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param v
             */
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(PatientEnroll.this, date,myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                // new DatePickerDialog(this,)
            }
        });

        ImageButton bckButton = (ImageButton) findViewById(R.id.patientEnrlBackBtn);
        bckButton.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param view
             */
            @Override
            public void onClick(View view) {
                Intent i = new Intent(PatientEnroll.this, MainActivity.class);
                i.putExtra(getResources().getString(R.string.placehoder_enum), PlaceholderState.NULL);
                finish();
                startActivity(i);
            }
        });

        Button loginButton = (Button) findViewById(R.id.patientLoginBtn);
        loginButton.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param view
             */
            @Override
            public void onClick(View view) {
                if(requestDataModel != null){
                    requestDataModel = null;
                    requestDataModel = new RequestDataModel();
                    Log.d("requestDataModel from PatientEnroll.java...","requestDataModel from PatientEnroll.java");
                }
                Context lCtx = mCtx;
                Intent i=null;
                String name=patientName.getText().toString();
                String countryCode = extractCountryCodeFromDropdown(patientCountry.getSelectedItem().toString());
                String phone = patientPhone.getText().toString();
                String dob=extractDOBInLocalFormat(patientDOB.getText().toString());
//                if((name.length()==0) && (phone.length() == 0) && (dob.length() == 0)) return;
                Log.d("Patient value",name);
                StringBuffer hcwguid=new StringBuffer();

                Map<String,String> searchParams = new HashMap<String,String>();
                if (name.length() != 0)
                    searchParams.put("name",name);
                if (phone.length() != 0)
                    searchParams.put("phone",phone);
                if (countryCode.length() != 0)
                    searchParams.put("countryCode",countryCode);
                if (dob.length() != 0)
                    searchParams.put("DOB",dob);
                searchParams.put("indType","HCW");
                StringBuffer missingFields = new StringBuffer();
                //   int userpreset = (IdentityModule.getInstance(mCtx)).isUserPresent(name,searchParams,hcwguid,missingFields);
                StringBuffer guid = new StringBuffer();

                String identityJSON = "{ \"resourceType\":\"IdentityRequest\", \"reqID\":\"101\", \"reqTxt\":\"AddPerson\", \"Inputs\": { \"currCtx\":\""+SessionProperties.getSessionInstance(mCtx).getSessionCtx()+"\", \"personType\":\"Patient\", \"skill\":\"\" } }";

                //      Intent sendIntent = new Intent();
                //    sendIntent.setAction("org.iprd.identity.IdmService");
                sendIntent = new Intent();
                sendIntent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                sendIntent.setAction("org.iprd.identity.IdmService");
                sendIntent.putExtra("IdentityRequest",identityJSON);
                sendIntent.setType("text/json");
                Intent chooser = Intent.createChooser(sendIntent, "Identity svc");
                if (sendIntent.resolveActivity(getPackageManager()) != null) {
                    if(Utils.GetUtils().CheckIfMessageTosend()) {
                        Log.d("idJSON", identityJSON);
                        startActivityForResult(chooser, 66);
                    }
                }
                //setButtonVisibility(R.id.patientEnrlNextBtn,View.VISIBLE);
                //setButtonVisibility(R.id.patientLoginBtn,View.INVISIBLE);
                //int userpreset = IdentityManager.getIDMInstance().AddPerson(currCtx.toString(),"",mCtx,name,countryCode,phone,dob,"","","","HCW",null,"","",guid,missingFields);
                //SessionProperties.getSessionInstance().setPatientGUID(guid.toString());
                //SessionProperties.getSessionInstance().setPatientName(name);
            }
        });



        nxtButton.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param view
             */
            @Override
            public void onClick(View view) {
                Intent i = new Intent(
                        PatientEnroll.this,
                        EmptyPlaceholders.class);
                i.putExtras(getIntent());
                i.putExtra(getResources().getString(R.string.placehoder_enum), PlaceholderState.PATIENT_DEVICES_MEASUREMENT);
                finish();
                startActivity(i);
                }
        });
        ImageButton login = (ImageButton) findViewById(R.id.hcwLgnPatientEnrl);
        login.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param view
             */
            @Override
            public void onClick(View view) {
                Intent i = new Intent(PatientEnroll.this, MainActivity.class);
                finish();
                startActivity(i);
            }
        });
        ImageButton exitout = (ImageButton) findViewById(R.id.exitPatientEnrl);
        exitout.setOnClickListener(new View.OnClickListener() {
            /**
             *
             * @param view
             */
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    private void updateLabel(EditText edittext,Calendar myCalendar) {
        String myFormat = "dd/MM/yyyy"; //date format used in Africa.
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        edittext.setText(sdf.format(myCalendar.getTime()));
    }

    private String extractDOBInLocalFormat(String dateUIFormat) {
        return (dateUIFormat.replace("/",""));
    }

    private String extractCountryCodeFromDropdown(String dropdownStr) {
        //loop thru string and extract number between + and )
        //ex: India(+91)
        String actualCountryCode = "";
        boolean numComing = false;
        for (int i=0;i<dropdownStr.length();i++) {
            char c = dropdownStr.charAt(i);
            if (numComing)
            {
                if (Character.isDigit(c))
                    actualCountryCode = actualCountryCode+Character.toString(c);
                else if (c == CLOSING_BRACE)
                {
                    numComing = false;
                    return actualCountryCode;
                }
                else
                {
                    //while parsing numbers, we should either encounter a numeric value
                    //or we should encounter a closing brace - indicating we're done reading the number
                    //anything other than that means this is possibly bad data so return null
                    return null;
                }
            }
            else if (c == PHONE_INDICATOR)
            {
                numComing = true;
            }
        }
        return null;
    }

    private void setButtonVisibility(int btnid, int visibility) {
        Button b = (Button) findViewById(btnid);
        b.setVisibility(visibility);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == 66) {
            Log.i("ActRes","Ok-66");
            if (data.hasExtra("IdentityResponse")) {
                String t = data.getExtras().getString("IdentityResponse");
                try {
                    JSONObject tOut =new JSONObject(t);
                    String apiType = tOut.getString("reqTxt");
                    if (apiType.equals(ADD_PERSON)) {
                        String guid = tOut.getJSONObject("Outputs").getString("guid");
                        String dob = tOut.getJSONObject("Outputs").getString("dob");
                        String success = tOut.getJSONObject("Outputs").getString("addDone");
                        if (success.length() > 0 && success.equals("true")) {
                        //if (missingFields == null || missingFields.length() == 0) {
                            String name = tOut.getJSONObject("Outputs").getString("name");
                            if (LoginProperties.getLoginInstance(mCtx).getpName() == null)
                                LoginProperties.getLoginInstance(mCtx).setpName(name);
                            Log.d("DOB from IdM",dob.toString());
                            LoginProperties.getLoginInstance(mCtx).setPDOB(dob);
                            LoginProperties.getLoginInstance(mCtx).setPGUID(guid);
                            //LoginProperties.getLoginInstance(mCtx).setpGender(gender);

                            setButtonVisibility(R.id.patientEnrlNextBtn,View.VISIBLE);
                            setButtonVisibility(R.id.patientLoginBtn,View.INVISIBLE);
                            nxtButton.callOnClick();
                        }
                        Toast.makeText(this,"Recvd addJSON result",Toast.LENGTH_LONG).show();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(this,"Failure",
                            Toast.LENGTH_LONG).show();
                }
                //      Toast.makeText(this,t,
                //            Toast.LENGTH_LONG).show();
            }
            else
            {
                Toast.makeText(this,data.getDataString(),Toast.LENGTH_LONG).show();
            }
        }
        else if (resultCode == RESULT_CANCELED)
        {
            Toast.makeText(this,"Failure in API call. Please check the data you are sending",Toast.LENGTH_LONG).show();
        }
    }

    /**
     * onBackPressed() :  pop-up dialog for exit the MGD app or cancel the pop-up dialog box.
     */
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
        .setIcon(android.R.drawable.ic_dialog_alert)
        .setTitle("Exiting MGD")
        .setMessage("Are you sure you want to exit this application?")
        .setPositiveButton("Yes", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finishAndRemoveTask();
                finish();
            }
        })
        .setNegativeButton("No", null)
        .show();
    }

}
