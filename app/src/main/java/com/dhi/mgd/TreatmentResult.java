package com.dhi.mgd;
/**
 *.
 * Date: Aug 30 2019
 * @author Apra Developers
 * @version 1.0
 */
public class TreatmentResult {
    public String Name, Catagory;
    public long ID;

    protected TreatmentResult() {
        ID = -1;
        Name = "";
        Catagory = "";
    }
}