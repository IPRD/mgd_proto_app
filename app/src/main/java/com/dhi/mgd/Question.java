package com.dhi.mgd;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
/**
 *.
 * Date: Aug 30 2019
 * @author Apra Developers
 * @version 1.0
 */
public class Question {
    private String GroupID;
    private String ID;
    private String Text;
    private QuestionType Type;
    private LinkedHashMap <String, String> Options;
    private ArrayList<String> arrayResponse;
    private int numResponse;
    private double realResponse;
    private String strResponse;

    public Question(String groupID, String questionID, String questionText, QuestionType questionType, LinkedHashMap<String, String> questionVal) {
        this.GroupID = groupID;
        this.ID = questionID;
        this.Text = questionText;
        this.Type = questionType;
        this.Options = questionVal;
        arrayResponse = new ArrayList<String>();
    }

    String getGroupID() {
        return GroupID;
    }

    private void setGroupID(String groupID) {
        this.GroupID = groupID;
    }

    String getID() {
        return ID;
    }

    private void setID(String ID) {
        this.ID = ID;
    }

    public String getText() {
        return Text;
    }

    public void setText(String text) {
        this.Text = text;
    }

    public QuestionType getType() {
        return Type;
    }

    public void setType(QuestionType type) {
        this.Type = type;
    }

    public LinkedHashMap <String, String> getOptions() {
        return Options;
    }

    public void setOptions(LinkedHashMap <String, String> options) {
        this.Options = options;
    }

    public ArrayList<String> getArrayResponse() {
        return arrayResponse;
    }

    public void setArrayResponse(ArrayList<String> arrayResponse) {
        this.arrayResponse = arrayResponse;
    }

    public int getNumResponse() {
        return numResponse;
    }

    public void setNumResponse(String keyUnit, int numResponse) {
        if (this.arrayResponse != null)
            this.arrayResponse.clear();
        this.arrayResponse = new ArrayList<String>();
        this.arrayResponse.add(keyUnit);
        this.numResponse = numResponse;
    }

    public double getRealResponse() {
        return realResponse;
    }

    public void setRealResponse(String keyUnit, double realResponse) {
        if (this.arrayResponse != null)
            this.arrayResponse.clear();
        this.arrayResponse = new ArrayList<String>();
        this.arrayResponse.add(keyUnit);
        this.realResponse = realResponse;
    }

    public String getStrResponse() {
        return strResponse;
    }

    public void setStringResponse(String keyUnit, String strResponse) {
        if (this.arrayResponse != null)
            this.arrayResponse.clear();
        this.arrayResponse = new ArrayList<String>();
        this.arrayResponse.add(keyUnit);
        this.strResponse = strResponse;
    }
}
