package com.dhi.mgd;

import android.content.Context;
import android.content.SharedPreferences;
/**
 *.
 * Date: Aug 30 2019
 * @author Apra Developers
 * @version 1.0
 */
public class SessionProperties {
    private String mSessionCtx;
    private String mHcwGUID;
    private String mPatientGUID;
    private String mHcwPhone;
    private String mHcwCC;
    private String mPatientPhone;
    private String mPatientCC;
    private String mHcwName;
    private String mPatientName;
    private static SessionProperties mInstance = null;
    private Context mCtx;
    private SharedPreferences mPref;
    private SharedPreferences.Editor mEdit;

    private SessionProperties() {
    }
    private SessionProperties(Context ctx) {
        initSharedPrefs(ctx);
    }

    private void initSharedPrefs(Context ctx)
    {
        mCtx = ctx;
        mPref = ctx.getSharedPreferences("IdMSession", Context.MODE_PRIVATE);
        mEdit = mPref.edit();
    }

    private void sessionState(StringBuffer userName, boolean getFn,boolean hcwUser,String stateVar) {
        String dataKey = "";
        //following state vars are not tied to hcw/patient and are common
        if (stateVar.equals("Location") || stateVar.equals("POS") || stateVar.equals("SessionCtx"))
            dataKey = stateVar;
        if (hcwUser)
            dataKey = "hcw"+stateVar;
        else
            dataKey = "patient"+stateVar;

        if (getFn) {
            String exValue = "";
            try {
                userName.append(mPref.getString(dataKey, exValue));
            } catch (ClassCastException e) {
                userName.append(exValue);
            }
        } else {
            mEdit.putString(dataKey,userName.toString());
            mEdit.commit();
        }
        return;
    }

    public String getSessionCtx() {
        StringBuffer sessionCtx = new StringBuffer();
        sessionState(sessionCtx,true,true,"SessionCtx");
        return sessionCtx.toString();
    }

    public void setSessionCtx(String newSessionCtx) {
        StringBuffer sessionCtx = new StringBuffer();
        sessionCtx.append(newSessionCtx);
        sessionState(sessionCtx,false,true,"SessionCtx");
    }

    public String getHcwGUID() {
        return mHcwGUID;
    }

    public void setHcwGUID(String hcwGUID) {
        this.mHcwGUID = hcwGUID;
    }

    public void setHcwName(String hcwName) { mHcwName = hcwName; }

    public String getHcwName() { return mHcwName; }

    public void setPatientName(String patientName) {
        StringBuffer pName = new StringBuffer();
        pName.append(patientName);
        sessionState(pName,false,false,"Name");
    }

    public String getPatientName() {
        StringBuffer pName = new StringBuffer();
        sessionState(pName,true,false,"Name");
        return pName.toString();
    }

    public String getPatientGUID() {
        return mPatientGUID;
    }

    public void setPatientGUID(String patientGUID) {
        this.mPatientGUID = patientGUID;
    }

    public String getHcwPhone() {
        return mHcwPhone;
    }

    public void setHcwPhone(String hcwPhone) {
        this.mHcwPhone = hcwPhone;
    }

    public String getHcwCC() {
        return mHcwCC;
    }

    public void setHcwCC(String hcwCC) {
        this.mHcwCC = hcwCC;
    }

    public String getPatientPhone() {
        return mPatientPhone;
    }

    public void setPatientPhone(String patientPhone) {
        this.mPatientPhone = patientPhone;
    }

    public String getPatientCC() {
        return mPatientCC;
    }

    public void setPatientCC(String patientCC) {
        this.mPatientCC = patientCC;
    }

    public static synchronized SessionProperties getSessionInstance(Context ctx) {
        if (mInstance == null)
            mInstance = new SessionProperties(ctx);
        return mInstance;
    }
}
