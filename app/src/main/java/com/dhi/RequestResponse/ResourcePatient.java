package com.dhi.RequestResponse;


import java.util.ArrayList;
import java.util.HashMap;
/**
 *.
 * Date: Aug 30 2019
 * @author Apra Developers
 * @version 1.0
 */
public class ResourcePatient {
    public String id;
    public String resourceType = "Patient";
    public String gender;
    public String birthDate;
    public HashMap<String, String> text = new HashMap<>();
    public Boolean active;
    public ArrayList<Identifier> identifier = new ArrayList<>();
    public ArrayList<Name> name = new ArrayList<>();

}
