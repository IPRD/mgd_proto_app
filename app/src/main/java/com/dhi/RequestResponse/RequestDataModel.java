package com.dhi.RequestResponse;

import java.util.ArrayList;
/**
 *.
 * Date: Aug 30 2019
 * @author Apra Developers
 * @version 1.0
 */
public class RequestDataModel {
    public String resourceType = "Bundle";
    public String type = "collection";
    public ArrayList<Entry> entry = new ArrayList<>();
}


