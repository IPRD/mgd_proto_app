package com.dhi.RequestResponse;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.databind.annotation.JsonValueInstantiator;

import java.util.ArrayList;
import java.util.HashMap;
/**
 *
 * Date: Aug 30 2019
 * @author Apra Developers
 * @version 1.0
 */
public class ResourceEncounter {
    public String id;
    public String resourceType = "Encounter";
    public HashMap<String, String> text = new HashMap<>();
    public ArrayList<Identifier> identifier = new ArrayList<>();
    public String status;
    @JsonProperty("class")
    public HashMap <String, String> Class = new HashMap<>();
    public HashMap <String, String> subject = new HashMap<>();
    public ArrayList<Individual> participant = new ArrayList<>();
    public ArrayList<Location> location = new ArrayList<>();
    public ArrayList<ResourceLocation> resourceLocation = new ArrayList<>();
    public ArrayList<ResourceLocation> contained = new ArrayList<>();

}



