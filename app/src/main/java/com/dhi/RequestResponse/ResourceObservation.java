package com.dhi.RequestResponse;

import java.util.ArrayList;
import java.util.HashMap;
/**
 *.
 * Date: Aug 30 2019
 * @author Apra Developers
 * @version 1.0
 */
public class ResourceObservation {
    public String id;
    public String resourceType = "Observation";
    public String status;
    public String effectiveDateTime;
    public ArrayList<BasedOn> basedOn = new ArrayList<>();
    public Code code = new Code();
    public ValueQuantity valueQuantity = new ValueQuantity();

    public ArrayList<Identifier> identifier = new ArrayList<>();
    public Subject subject;
}






