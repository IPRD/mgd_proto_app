package com.dhi.RequestResponse;

import java.util.ArrayList;
import java.util.HashMap;
/**
 *.
 * Date: Aug 30 2019
 * @author Apra Developers
 * @version 1.0
 */
public class Name {
    public String family;
    public HashMap<String, ArrayList<String>> mapA = new HashMap<>();
    public ArrayList<String> given = new ArrayList<String>();
}
