package com.dhi.RequestResponse;

import java.util.ArrayList;
import java.util.HashMap;
/**
 *.
 * Date: Aug 30 2019
 * @author Apra Developers
 * @version 1.0
 */
public class ResourcePractitioner {
    public String id;
    public String resourceType = "Practitioner";
    public ArrayList<Identifier> identifier = new ArrayList<>();
    public Boolean active;
    public ArrayList<Name> name = new ArrayList<>();
    public HashMap<String,Object> qualification = new HashMap<String,Object>();
}
