package com.dhi.RequestResponse;
/**
 *.
 * Date: Aug 30 2019
 * @author Apra Developers
 * @version 1.0
 */
public class Coding{
    public String system;
    public String code;
    public String display;
    public Coding(String system,String code,String display){
        this.system = system;
        this.code = code;
        this.display = display;
    }
}