package com.dhi.RequestResponse;
/**
 *.
 * Date: Aug 30 2019
 * @author Apra Developers
 * @version 1.0
 */
public class ValueQuantity{
    public float value;
    public String unit;
    public String code;
    public String system;
}