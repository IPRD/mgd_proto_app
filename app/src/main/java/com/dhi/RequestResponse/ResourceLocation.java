package com.dhi.RequestResponse;

import java.util.HashMap;
/**
 *.
 * Date: Aug 30 2019
 * @author Apra Developers
 * @version 1.0
 */
public class ResourceLocation {
   public String resourceType;
   public String id;
   public HashMap<String, String> text = new HashMap<>();
   public String status;
   public String name;
   public String description;
   public String mode;
   public PhysicalType physicalType;

}
