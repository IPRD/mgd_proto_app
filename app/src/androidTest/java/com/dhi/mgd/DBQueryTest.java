package com.dhi.mgd;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.dhi.mgd.ProofOfService.CreateProofImage;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import com.cloudant.sync.query.FieldSort;
import com.cloudant.sync.query.Index;
import com.cloudant.sync.query.QueryException;
import com.dhi.mgd.IdentityModule;

//import static android.graphics.Paint.Align.CENTER;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class DBQueryTest {
    @Test
    public void setupDBQuery() {
      //Invoke ur Identity.
        IdentityModule idm = IdentityModule.getInstance(InstrumentationRegistry.getTargetContext());
        List<String> fields = new ArrayList<String>();
        fields.add("name");
        fields.add("indType");
        StringBuffer guiddata = new StringBuffer();

        Map<String,String> searchParams = new HashMap<String,String>();
        searchParams.put("name","Peter Pan");
        searchParams.put("idType","HCW");
        if (!idm.isIndexingDone()) {
            boolean indexed = idm.indexData(fields);
            assertEquals(true, indexed);
        }
        else
        {
            boolean rmIndex = idm.rmAllIndices();
            assertEquals(true,rmIndex);
            idm.rmIndex("idNameIndex");
            boolean indexed = idm.indexData(fields);
            assertEquals(true, indexed);
        }

        idm.addIndividual("Peter Pan","HCW","32","Male","Surgeon");
        idm.addIndividual("Peter Pan","Patient","37","Male","");

        boolean user = idm.isUserPresent("Peter Pan",searchParams,guiddata);
        boolean user1 = idm.isUserPresent("Peter Pan",guiddata);
        assertEquals(true,user);

        List<Index> indList = new ArrayList<Index>();

        boolean listFlag = idm.getIndices(indList);
        assertEquals(true,listFlag);

        for (int i=0;i<indList.size();i++)
        {
            String indName = indList.get(i).toString();
            boolean rmFlag = idm.rmIndex(indName);
        }


        //SimpleQuery
      //  assertEquals("com.dhi.mgd", appContext.getPackageName());
    }

}
