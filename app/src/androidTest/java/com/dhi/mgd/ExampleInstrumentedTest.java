package com.dhi.mgd;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.dhi.identitymodule.MyProperties;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import static com.dhi.mgd.ProofOfService.CreateProofImage;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

//import static android.graphics.Paint.Align.CENTER;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void AssesmentTest() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();
        assertEquals("com.dhi.mgd", appContext.getPackageName());

        MyProperties.getInstance().LoadJsonForAssesment(appContext);
        ArrayList<Question> assesment= MyProperties.getInstance().mAssesments;

        assertEquals("assesment size", assesment.size(), 5);

        MyProperties.getInstance().LoadQuestions(appContext);
        ArrayList<Question> q=MyProperties.getInstance().FindQuestions();
        assertEquals("question size", q.size(), 31);

    }
    @Test
    public void useAppContext() {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();
        assertEquals("com.dhi.mgd", appContext.getPackageName());
        MyProperties.getInstance().LoadQuestions(appContext);

        ArrayList<Question> questions=new ArrayList<Question>();
        if( MyProperties.getInstance().mQuestions.size()==0) {
            {
                ArrayList<Question> q = JSONGenerator.getQuestions(MyProperties.getInstance().mQuestionStr, "personal", "", "");
                for (int k = 0; k < q.size(); k++) {
                    System.out.println(q.get(k).toString());
                }
                questions.addAll(q);
            }
            {
                ArrayList<Question> q = JSONGenerator.getQuestions(MyProperties.getInstance().mQuestionStr, "vitals", "", "");
                for (int k = 0; k < q.size(); k++) {
                    System.out.println(q.get(k).toString());
                }
                questions.addAll(q);
            }
            {
                ArrayList<Question> q = JSONGenerator.getQuestions(MyProperties.getInstance().mQuestionStr, "symptoms", "", "");
                for (int k = 0; k < q.size(); k++) {
                    System.out.println(q.get(k).toString());
                }
                questions.addAll(q);
            }
            MyProperties.getInstance().mQuestions = questions;
        }
    }

    public Bitmap createImage(int w, int h, int color) {
        Bitmap b = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        c.drawColor(color);
        Paint p;
        Paint pText = new Paint();
        pText.setColor(Color.BLACK);
        c.drawCircle(w / 2, h / 2, w / 4, pText);
        return b;
    }

    @Test
    public void ImageMerging() {
        int hcwWidth = 480, hcwHeight = 640;
        String message = "This is the Transaction string I have to test it this if first line 1 \nThis is the Transaction string I have to test it this if first line 2 \nThis is the Transaction string 3 \nThis is the Transaction string 4 \nThis is the Transaction string 5 \nThis is the Transaction string 6 \n ";

        try {
            Bitmap hcw = createImage(hcwWidth / 3, hcwHeight / 3, Color.BLUE);
            Bitmap patient = createImage(hcwWidth, hcwHeight, Color.DKGRAY);
            Bitmap sign = createImage(hcwWidth, hcwHeight / 4, Color.YELLOW);
            Bitmap ret = CreateProofImage(message, hcw, patient, sign);
            assertEquals("Final Width", ret.getWidth(), 1080);
            assertEquals("Final Height", ret.getHeight(), 1920);

            // return finalBitmap;
        } catch (OutOfMemoryError e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        } catch (Exception ex) {
            ex.printStackTrace();
            String t = ex.toString();
            System.out.println(t);
        }
    }

    @Test
    public void parsingJSONTest() {
        Context context = InstrumentationRegistry.getTargetContext();
        String jsonStr = JSONGenerator.loadJSONFromAsset(context, R.string.hcw_guid);
        assertNull("With Dummy filename should return null", jsonStr);
        jsonStr = JSONGenerator.loadJSONFromAsset(context, R.string.questions_json);
        assertNotNull("Reading of questions failed", jsonStr);
        ArrayList<Question> questionArray = JSONGenerator.getQuestions(jsonStr, "miser","", "");
        assertTrue("Should not get any questions", questionArray.size() == 0);
        questionArray = JSONGenerator.getQuestions(jsonStr, "malaria","", "");
        assertTrue("Should parse questions", questionArray.size() > 0);
    }
}
