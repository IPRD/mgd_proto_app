package com.dhi.mgd;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.cloudant.sync.query.Index;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

//import static android.graphics.Paint.Align.CENTER;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class IdentityTest {
    private IdentityModule idm;
    public IdentityTest() {
        idm = IdentityModule.getInstance(InstrumentationRegistry.getTargetContext());
    }

    @Test
    public void testReplicator() {
        boolean repData = idm.replicateData();
        assertEquals(true,repData);
    }

}
