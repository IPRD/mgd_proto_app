# mgd_proto_app

## Instructions for build

1. Please transfer the Idm-gen-326.apk to your android device and install. To verify if the installation is successful, go to the Settings->Apps section on the android device and check for an application by the name of Idm.

2. Open the mgd_prot_app project in android studio and build on your android device

3. To use the third party functionalities such as bluetooth objective measurement devices, finger print biometrics etc please refer to the table below

| Third party Component 	| Link to acquire 	| What it is used for 	| What if it is not present 	|
|-	|-	|-	|-	|
| Nuero SDK 	| https://www.neurotechnology.com/verifinger.html 	| Used to match finger print biometerics 	| Identity manager will start working but matching will be based only on demographics 	|
| Jumper Bluetooth thermometer 	| http://www.jumper-medical.com/en/pros_d.aspx?CateId=328&sCateId=338&pid=274 	| Used to measure body temperature 	| You will have to fill the observations in a text field 	|
| Jumper Bluetooth SpO2 and heart rate monitor 	| http://www.jumper-medical.com/en/pros_d.aspx?CateId=354&sCateId=356&pid=348 	| Used to measure SpO2 levels and heart rate 	| You will have to fill the observations in a text field 	|
| Healthcubed 	| https://healthcubed.com/ 	| Used to measure a wide variety of Objective measures 	| You will have to fill the observations in a text field 	|
| USB finger print scanner 	| https://integratedbiometrics.com/products/watson-mini/ 	| Used to acquire finger print biometrics 	| Finger print biometerics will not be supported 	|
| Heart rate monitor -Conrad tucker 	| https://www.videovitals.org/ 	| Used to acquire heart rate based on a software system only 	| You will have to fill the observations in a text field 	|
| ThinkMD 	| https://thinkmd.org/ 	| Used as a blackbox triaging tool which asks questions and returns possible disease with adviced medication 	| You will not have this capability. We are currently talking to them to rerun their test server. 	|


## Barebones functionality

1. Ability to login as a Health Care Worker(HCW). The system automatically enrolls you as a HCW if a match is not found in the Data base.

2. Once logged in, you will be able to enroll a new patient encounter and go through the workflow of collecting objective measures, subjective measures, triaging and medicine dispense.

3. Once enrolled, the system has a local database which stores demographics and biometrics and also syncs with the cloud. If you enter same details in a new encounter, the system will prompt a match.

## Notes about applications

1. You will notice that the Pulse & SpO2 and Temperature show as available devices. This is because they only need phone bluetooth to connect to the device and do not require any third party application to be installed on the phone.

2. The subjective assessment will not work . 